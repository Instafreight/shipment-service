<?php

use App\Http\Controllers\V1\ShipmentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function (){
    Route::get('shipments', [ShipmentController::class, 'index'])->name('shipments.list');

    Route::post('shipments/import', [ShipmentController::class, 'import'])
        ->middleware(['basic', 'throttle:import'])
        ->name('shipments.import');

    Route::post('shipments/import-sync', [ShipmentController::class, 'importSync'])
        ->middleware(['basic', 'throttle:import'])
        ->name('shipments.import-sync');
});
