<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('distance');
            $table->unsignedInteger('time');
            $table->unsignedDouble('price')->nullable();
            $table->unsignedBigInteger('source_id')->unique()->nullable();

            $table->foreignId('company_id')->references('id')->on('companies')->cascadeOnDelete();
            $table->foreignId('carrier_id')->references('id')->on('carriers')->cascadeOnDelete();
            $table->foreignId('start_route_id')->references('id')->on('routes')->cascadeOnDelete();
            $table->foreignId('end_route_id')->references('id')->on('routes')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipments');
    }
};
