# Develop Setup
The easier way of running the app is to use docker and docker compose.

The steps for initial setup are:
- Run `docker-compose up` at the project root.
- Run `docker-compose run app composer install` at the project root.
- Run `docker-compose run app php artisan key:generate` at the project root.
- Run `docker-compose run app php artisan migrate` at the project root.
- Run `docker-compose run app php artisan db:seed` at the project root.

From next time you can just go with `docker-compose up` and when you need migration or seed, just run the necessary commands.
Then just open [localhost:8080](http://localhost:8080) to view the app.

### Makefile commands
For ease of use when working with docker, we have created several make commands as following:
- `make up`: Build and run the containers
- `make down`: Stop the containers
- `make update`: Short for `composer install`
- `make status`: Short for `docker-compose ps`
- `make destroy`: Remove all orphaned containers
- `make shell`: Get shell access into app container
- `make consume`: Consume worker services (PHP & Python)
- `make prepare`: Migrate and clear the app cache
- `make run-tests`: Run PHPUnit tests
