# Technology Stack
The application is developed using Laravel framework as REST API.

### Services
The technologies with the tested versions are:

| Tech           | Version  | Usage                                                 |
|----------------|----------|-------------------------------------------------------|
| PHP            | 8.1.7    | Main programming language used in the project.        |
| Python         | 3.9.2    | Used for additional shipment consumer implementation. |
| Laravel        | 9.19     | Main framework used in the project.                   |
| MySQL          | 8.0      | Main Database used in the project.                    |
| RabbitMQ       | 3.10.6   | Used as message broker for making async importer.     |
| Supervisor     | 4.2.2    | Used to keep the importer consumers run continuously. |
| Docker         | 20.10.17 | Virtualization Technology used in the project.        |
| Docker compose | 1.29.2   | Used to define multi-container applications.          |

