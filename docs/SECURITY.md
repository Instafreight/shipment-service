# Security
Several security measures are included in the project.

### Basic Authorization
To access some endpoints you need authorization. These routes are:
- POST: `/api/v1/shipments/import`
- POST: `/api/v1/shipments/import-sync`

Since there is no user auth mechanism in the system, basic fixed token authentication is used. 
To use it, just add a security header with key `X-INSTAFREIGHT-KEY` and value which is inside 
the `.env` file with key `BASIC_TOKEN_AUTH_API_KEY`.

### Throttling
For reducing abusive users, a throttling is included with the following endpoints:
- POST: `/api/v1/shipments/import`
- POST: `/api/v1/shipments/import-sync`

You can just send `10` of these requests in a minute.
