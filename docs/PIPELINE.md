# PIPELINE
The pipeline currently is based on Gitlab CI service.

### Backend pipeline
Backend pipeline flow is as following:
1. Fetch code changes from repository (for staging the branch `stage`).
2. Run the tests using `phpunit`.
3. Use `dpl` tool to upload the codes to `Heroku App`.
4. Build the heroku app and deploy it.

In case if the pipeline fails it will show inside the gitlab project Pipelines.
