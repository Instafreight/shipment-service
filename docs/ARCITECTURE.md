# Software Architecture
The application is developed using Laravel framework as REST API.

Application is separated into several parts:
- Controllers go inside the `app\Http\Controllers`
- Middlewares go inside the `app\Http\Middlewares`
- Requests go inside the `app\Http\Requests`
- Services go inside the `app\Services`
- Migrations go inside the `database\migrations`
- Seeders go inside the `database\seeders`
- Deploy config go inside the `deploy`
- Documents go inside the `docs`
- Routes go inside the `routes`
- Tests go inside the `tests`

### Services
Application is segregated into several services that can be called from anywhere in the application.
The services are all inside `App\Services` and are:
- Base: Basic implementation of necessary classes.
- Carrier: Handling Carriers.
- Company: Handling Companies.
- Region: Handling Regions including cities and countries.
- Route: Handling Routes.
- Shipment: Handling shipments.
- ShipmentImport: Handling shipment importing.

### General Service Architecture
Each service has the following general structure:
- Commands: Console commands go here
- Concretes: Concrete implementations of different utility classes
- Contracts: Contracts/interfaces and abstractions
- Exceptions: Service Exceptions
- Helpers: Helper classes used in service
- Models: Models related to that service
- Producers: Queue Producer classes used in service
- Repositories: Repositories used in the service
- Validations: Validation classes used inside the services

Also each service contains a `ServiceProvider` that registers the stuff related in the service.
