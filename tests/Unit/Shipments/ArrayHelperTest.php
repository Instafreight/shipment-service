<?php

namespace Tests\Unit\Shipments;

use App\Services\ShipmentImporter\Helpers\ArrayHelper;
use PHPUnit\Framework\TestCase;

class ArrayHelperTest extends TestCase
{
    public function testArrayHelper_WhenIsDifferentIsCalled_ItReturnsBool()
    {
        $this->assertIsBool(ArrayHelper::isDifferent([], []));
    }

    public function testArrayHelper_WhenIsDifferentIsCalledWithDifferentInput_ItReturnsTrue()
    {
        $this->assertTrue(ArrayHelper::isDifferent([1], [2]));
    }

    public function testArrayHelper_WhenIsDifferentIsCalledWithSameInput_ItReturnsFalse()
    {
        $this->assertFalse(ArrayHelper::isDifferent([1], [1]));
    }

    public function testArrayHelper_WhenIsDifferentIsCalledWithEmptyArrays_ItReturnsFalse()
    {
        $this->assertFalse(ArrayHelper::isDifferent([], []));
    }
}
