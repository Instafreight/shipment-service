<?php

namespace Tests\Unit\Shipments;

use App\Services\Base\Exception\AppException;
use App\Services\ShipmentImporter\Helpers\JsonHelper;
use PHPUnit\Framework\TestCase;

class JsonHelperTest extends TestCase
{
    public function testJsonHelper_WhenHashIsCalledWithProperStringData_ItReturnsString()
    {
        $this->assertIsString(JsonHelper::hash("test", 'sha1'));
    }

    public function testJsonHelper_WhenHashIsCalledWithProperStringDataAndHashAlgorithm_ItReturnsCorrectResult()
    {
        $this->assertEquals(
            "a94a8fe5ccb19ba61c4c0873d391e987982fbbd3",
            JsonHelper::hash("test", 'sha1')
        );
    }

    public function testJsonHelper_WhenHashIsCalledWithArrayData_ItReturnsStringResult()
    {
        $this->assertIsString(JsonHelper::hash(["key" => "value"], 'sha1'));
    }

    public function testJsonHelper_WhenHashIsCalledWithIncorrectAlgorithm_ItThrowsException()
    {
        $this->expectException(AppException::class);

        JsonHelper::hash("", 'mohsen-hash:D');
    }
}
