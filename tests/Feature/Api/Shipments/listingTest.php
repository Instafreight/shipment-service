<?php

namespace Tests\Feature\Api\Shipments;

use App\Services\Shipment\Contracts\ShipmentServiceContract;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class listingTest extends TestCase
{
    use RefreshDatabase;

    public function testShipmentsApi_WhenGetEndpointIsCalled_ItReturnsSuccess()
    {
        $response = $this->json('get', '/api/v1/shipments');

        $response->assertStatus(200);
    }

    public function testShipmentsApi_WhenGetEndpointIsCalled_ItHasPaginationStructure()
    {
        $response = $this->json('get', '/api/v1/shipments');

        $response->assertJsonStructure([
            'current_page',
            'data',
            'first_page_url',
            'from',
            'last_page',
            'last_page_url',
            'links',
            'next_page_url',
            'path',
            'per_page',
            'prev_page_url',
            'to',
            'total',
        ]);
    }

    public function testShipmentsApi_WhenGetEndpointIsCalled_ItCallsForShipmentServicePaginateWithFilter()
    {
        $shipmentServiceMock = $this->mock(ShipmentServiceContract::class);
        $shipmentServiceMock->shouldReceive('paginateWithFilter')->once();

        $response = $this->json('get', '/api/v1/shipments');

        $response->assertStatus(200);
    }

    public function testShipmentsApi_WhenGetEndpointIsCalledWithFilter_ItCallsForShipmentServiceGetFilterMaps()
    {
        $shipmentServiceMock = $this->mock(ShipmentServiceContract::class);
        $shipmentServiceMock->shouldReceive('getFilterMaps')->once();
        $shipmentServiceMock->shouldReceive('paginateWithFilter')->once();

        $response = $this->json('get', '/api/v1/shipments?carrier_email=dummy@email.com');

        $response->assertStatus(200);
    }
}
