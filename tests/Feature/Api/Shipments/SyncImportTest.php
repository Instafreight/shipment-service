<?php

namespace Tests\Feature\Api\Shipments;

use App\Services\Base\Exception\AppException;
use App\Services\Base\Validation\JsonSchemaValidator;
use App\Services\ShipmentImporter\Contracts\ShipmentImporterServiceContract;
use App\Services\ShipmentImporter\Contracts\ShipmentProducerContract;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class SyncImportTest extends TestCase
{
    use RefreshDatabase;

    public function testShipmentsApi_WhenImportAsyncEndpointIsCalledWithoutAuth_ItThrowsForbiddenException()
    {
        $file = UploadedFile::fake()->create('document.json', 1024);
        $response = $this->json('post', '/api/v1/shipments/import-sync', [
            'import_file' => $file
        ]);

        $response->assertStatus(403);
    }

    public function testShipmentsApi_WhenImportAsyncEndpointIsCalledWithAuthWithoutPayload_ItThrowsValidationException()
    {
        $response = $this->json('post', '/api/v1/shipments/import-sync', [], [
            'X-INSTAFREIGHT-KEY' => env('BASIC_TOKEN_AUTH_API_KEY')
        ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(["import_file"]);
    }

    public function testShipmentsApi_WhenImportAsyncEndpointIsCalledWithProperPayload_ItCallsForShipmentImporterImport()
    {
        $producerMock = $this->mock(ShipmentImporterServiceContract::class);
        $producerMock->shouldReceive('import');

        $fake_file_path = base_path('tests/assets/fake_multiple_shipments.json');
        $file = UploadedFile::fake()->createWithContent('shipments.json', file_get_contents($fake_file_path));

        $response = $this->json('post', '/api/v1/shipments/import-sync', [
            "import_file" => $file
        ], [
            'X-INSTAFREIGHT-KEY' => env('BASIC_TOKEN_AUTH_API_KEY')
        ]);

        $response->assertStatus(200);
    }

    public function testShipmentsApi_WhenImportAsyncEndpointIsCalledWithProperPayload_ItReturnsNumberOfShipments()
    {
        $producerMock = $this->mock(ShipmentImporterServiceContract::class);
        $producerMock->shouldReceive('import');

        $fake_file_path = base_path('tests/assets/fake_multiple_shipments.json');
        $file = UploadedFile::fake()->createWithContent('shipments.json', file_get_contents($fake_file_path));

        $response = $this->json('post', '/api/v1/shipments/import-sync', [
            "import_file" => $file
        ], [
            'X-INSTAFREIGHT-KEY' => env('BASIC_TOKEN_AUTH_API_KEY')
        ]);

        $response->assertStatus(200);
        $response->assertExactJson(["importedCount" => 3]);
    }

    public function testShipmentsApi_WhenImportAsyncEndpointIsCalledWithProperPayload_ItCallsForJsonValidator()
    {
        $producerMock = $this->mock(JsonSchemaValidator::class);
        $producerMock->shouldReceive('validate');

        $fake_file_path = base_path('tests/assets/fake_multiple_shipments.json');
        $file = UploadedFile::fake()->createWithContent('shipments.json', file_get_contents($fake_file_path));

        $response = $this->json('post', '/api/v1/shipments/import-sync', [
            "import_file" => $file
        ], [
            'X-INSTAFREIGHT-KEY' => env('BASIC_TOKEN_AUTH_API_KEY')
        ]);

        $response->assertStatus(200);
    }

    public function testShipmentsApi_WhenImportAsyncEndpointIsCalledWithOneFaultyValidation_ItImportsJustTwo()
    {
        $producerMock = $this->mock(JsonSchemaValidator::class);
        $producerMock->shouldReceive('validate')->andReturn(false, true, true);

        $fake_file_path = base_path('tests/assets/fake_multiple_shipments.json');
        $file = UploadedFile::fake()->createWithContent('shipments.json', file_get_contents($fake_file_path));

        $response = $this->json('post', '/api/v1/shipments/import-sync', [
            "import_file" => $file
        ], [
            'X-INSTAFREIGHT-KEY' => env('BASIC_TOKEN_AUTH_API_KEY')
        ]);

        $response->assertStatus(200);
        $response->assertExactJson(["importedCount" => 2]);
    }

    public function testShipmentsApi_WhenImportAsyncEndpointIsCalledWithFaultyImport_ItImportsNothing()
    {
        $producerMock = $this->mock(ShipmentImporterServiceContract::class);
        $producerMock->shouldReceive('import')->andThrow(AppException::class);

        $fake_file_path = base_path('tests/assets/fake_multiple_shipments.json');
        $file = UploadedFile::fake()->createWithContent('shipments.json', file_get_contents($fake_file_path));

        $response = $this->json('post', '/api/v1/shipments/import-sync', [
            "import_file" => $file
        ], [
            'X-INSTAFREIGHT-KEY' => env('BASIC_TOKEN_AUTH_API_KEY')
        ]);

        $response->assertStatus(200);
        $response->assertExactJson(["importedCount" => 0]);
    }
}
