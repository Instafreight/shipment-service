<?php

namespace Tests\Feature\Api\Shipments;

use App\Services\Base\Validation\JsonSchemaValidator;
use App\Services\ShipmentImporter\Contracts\ShipmentProducerContract;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class AsyncImportTest extends TestCase
{
    use RefreshDatabase;

    public function testShipmentsApi_WhenImportAsyncEndpointIsCalledWithoutAuth_ItThrowsForbiddenException()
    {
        $file = UploadedFile::fake()->create('document.json', 1024);
        $response = $this->json('post', '/api/v1/shipments/import', [
            'import_file' => $file
        ]);

        $response->assertStatus(403);
    }

    public function testShipmentsApi_WhenImportAsyncEndpointIsCalledWithAuthWithoutPayload_ItThrowsValidationException()
    {
        $response = $this->json('post', '/api/v1/shipments/import', [], [
            'X-INSTAFREIGHT-KEY' => env('BASIC_TOKEN_AUTH_API_KEY')
        ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(["import_file"]);
    }

    public function testShipmentsApi_WhenImportAsyncEndpointIsCalledWithProperPayload_ItCallsForProducerDispatch()
    {
        $producerMock = $this->mock(ShipmentProducerContract::class);
        $producerMock->shouldReceive('dispatch');

        $fake_file_path = base_path('tests/assets/fake_multiple_shipments.json');
        $file = UploadedFile::fake()->createWithContent('shipments.json', file_get_contents($fake_file_path));

        $response = $this->json('post', '/api/v1/shipments/import', [
            "import_file" => $file
        ], [
            'X-INSTAFREIGHT-KEY' => env('BASIC_TOKEN_AUTH_API_KEY')
        ]);

        $response->assertStatus(200);
    }
}
