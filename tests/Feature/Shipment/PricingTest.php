<?php

namespace Tests\Feature\Shipment;

use App\Services\Shipment\Concretes\Pricing\PricingStrategyShort;
use App\Services\Shipment\Contracts\ShipmentRepositoryContract;
use App\Services\Shipment\Contracts\ShipmentServiceContract;
use App\Services\Shipment\Exceptions\InvalidShipmentDistanceException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PricingTest extends TestCase
{
    private $service;

    private $pricingAdjustment;

    private $shortUnitPrice;
    private $normalUnitPrice;
    private $longUnitPrice;
    private $veryLongUnitPrice;

    protected function setUp(): void
    {
        parent::setUp();
        $this->service = $this->app->make(ShipmentServiceContract::class);
        $this->pricingAdjustment = 0.00001;
        $this->shortUnitPrice = 0.3;
        $this->normalUnitPrice = 0.25;
        $this->longUnitPrice = 0.2;
        $this->veryLongUnitPrice = 0.15;
    }

    public function testShipmentPricing_WhenDistanceIsLessThanZero_ItThrowsInvalidDistanceException()
    {
        $this->expectException(InvalidShipmentDistanceException::class);

        $this->service->calculateShipmentPrice(-1);
    }

    public function testShipmentPricing_WhenDistanceIsShort_ItCalculatesCorrectly()
    {
        $calculatedPrice = $this->service->calculateShipmentPrice(50);

        $expectedPrice = $this->shortUnitPrice * $this->pricingAdjustment * 50;
        $this->assertEquals($expectedPrice, $calculatedPrice);
    }

    public function testShipmentPricing_WhenDistanceIs100_ItCalculatesAsShortCorrectly()
    {
        $calculatedPrice = $this->service->calculateShipmentPrice(100);

        $expectedPrice = $this->shortUnitPrice * $this->pricingAdjustment * 100;
        $this->assertEquals($expectedPrice, $calculatedPrice);
    }

    public function testShipmentPricing_WhenDistanceIsNormal_ItCalculatesCorrectly()
    {
        $calculatedPrice = $this->service->calculateShipmentPrice(150);

        $expectedPrice = $this->normalUnitPrice * $this->pricingAdjustment * 150;
        $this->assertEquals($expectedPrice, $calculatedPrice);
    }

    public function testShipmentPricing_WhenDistanceIs200_ItCalculatesAsNormalCorrectly()
    {
        $calculatedPrice = $this->service->calculateShipmentPrice(200);

        $expectedPrice = $this->normalUnitPrice * $this->pricingAdjustment * 200;
        $this->assertEquals($expectedPrice, $calculatedPrice);
    }

    public function testShipmentPricing_WhenDistanceIsLong_ItCalculatesCorrectly()
    {
        $calculatedPrice = $this->service->calculateShipmentPrice(250);

        $expectedPrice = $this->longUnitPrice * $this->pricingAdjustment * 250;
        $this->assertEquals($expectedPrice, $calculatedPrice);
    }

    public function testShipmentPricing_WhenDistanceIs300_ItCalculatesAsLongCorrectly()
    {
        $calculatedPrice = $this->service->calculateShipmentPrice(300);

        $expectedPrice = $this->longUnitPrice * $this->pricingAdjustment * 300;
        $this->assertEquals($expectedPrice, $calculatedPrice);
    }

    public function testShipmentPricing_WhenDistanceIsVeryLong_ItCalculatesCorrectly()
    {
        $calculatedPrice = $this->service->calculateShipmentPrice(350);

        $expectedPrice = $this->veryLongUnitPrice * $this->pricingAdjustment * 350;
        $this->assertEquals($expectedPrice, $calculatedPrice);
    }
}
