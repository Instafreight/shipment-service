<?php

namespace Tests\Feature\Shipment;

use App\Services\Base\Repository\Concretes\CriteriaCollection;
use App\Services\Base\Validation\Exception\ValidationException;
use App\Services\Shipment\Contracts\ShipmentRepositoryContract;
use App\Services\Shipment\Contracts\ShipmentServiceContract;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ShipmentServiceTest extends TestCase
{
    use WithFaker;

    private $repositoryMock;

    private $service;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repositoryMock = $this->mock(ShipmentRepositoryContract::class);
        $this->service = $this->app->make(ShipmentServiceContract::class);
    }

    public function testShipmentService_WhenAllIsCalled_ItCallsForRepositoryAll()
    {
        $this->repositoryMock->shouldReceive('all')->once();

        $this->service->all();
    }

    public function testShipmentService_WhenAllWithFilterIsCalled_ItCallsForRepositoryAllAndFilter()
    {
        $this->repositoryMock->shouldReceive('filter')->once()->andReturn($this->repositoryMock);
        $this->repositoryMock->shouldReceive('all')->once();

        $dummyCriteriaCollection = new CriteriaCollection();
        $this->service->allWithFilter($dummyCriteriaCollection);
    }

    public function testShipmentService_WhenFindByIdIsCalled_ItCallsForRepositoryFindById()
    {
        $dummyId = 1;
        $this->repositoryMock->shouldReceive('findById')->with($dummyId)->once();

        $this->service->findById(1);
    }

    public function testShipmentService_WhenFindBySourceIdIsCalled_ItCallsForRepositoryFindBySourceId()
    {
        $dummyId = 1;
        $this->repositoryMock->shouldReceive('findBySourceId')->with($dummyId)->once();

        $this->service->findBySourceId(1);
    }

    public function testShipmentService_WhenCreateIsCalledWithImproperData_ItThrowsValidationException()
    {
        $this->expectException(ValidationException::class);

        $dummyImproperData = [
            'key' => 'value'
        ];

        $this->service->create($dummyImproperData);
    }

    public function testShipmentService_WhenUpdateIsCalledWithImproperData_ItThrowsValidationException()
    {
        $this->expectException(ValidationException::class);

        $dummyId = 1;
        $dummyImproperData = [
            'distance' => 'string-value'
        ];

        $this->service->update($dummyId, $dummyImproperData);
    }

    public function testShipmentService_WhenUpdateIsCalledWithProperData_ItCallsForRepositoryUpdate()
    {
        $dummyId = 1;
        $dummyData = [];
        $this->repositoryMock->shouldReceive('update')->with($dummyId, $dummyData)->once();

        $this->service->update($dummyId, $dummyData);
    }

    public function testShipmentService_WhenUpdateIsCalledWithProperDistanceData_ItCallsForRepositoryUpdateWithPriceCalculation()
    {
        $dummyId = 1;
        $dummyData = [
            'distance' => 2000000
        ];
        $expectedData = array_merge($dummyData, ['price' => $this->service->calculateShipmentPrice($dummyData['distance'])]);

        $this->repositoryMock->shouldReceive('update')->with($dummyId, $expectedData)->once();

        $this->service->update($dummyId, $dummyData);
    }

    public function testShipmentService_WhenPaginateIsCalled_ItCallsForRepositoryPaginate()
    {
        $this->repositoryMock->shouldReceive('paginate')->once();

        $this->service->paginate();
    }

    public function testShipmentService_WhenPaginateIsCalledWithCustomPageInfo_ItCallsForRepositoryPaginateWithThoseInfo()
    {
        $this->repositoryMock->shouldReceive('paginate')->with(15, 3)->once();

        $dummyLimit = 15;
        $dummyPage = 3;
        $this->service->paginate($dummyLimit, $dummyPage);
    }

    public function testShipmentService_WhenPaginateWithFilterIsCalled_ItCallsForRepositoryPaginateAndFilter()
    {
        $this->repositoryMock->shouldReceive('filter')->once()->andReturn($this->repositoryMock);
        $this->repositoryMock->shouldReceive('enableEagerLoadRelations')->once()->andReturn($this->repositoryMock);
        $this->repositoryMock->shouldReceive('paginate')->once();

        $dummyCriteriaCollection = array();
        $dummyLimit = 15;
        $dummyPage = 3;
        $this->service->paginateWithFilter($dummyLimit, $dummyPage, $dummyCriteriaCollection);
    }
}
