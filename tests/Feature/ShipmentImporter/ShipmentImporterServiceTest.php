<?php

namespace Tests\Feature\ShipmentImporter;

use App\Services\Carrier\Contracts\CarrierServiceContract;
use App\Services\Company\Contracts\CompanyServiceContract;
use App\Services\Region\Contracts\CityServiceContract;
use App\Services\Region\Contracts\CountryServiceContract;
use App\Services\Region\Exceptions\CountryNotFoundException;
use App\Services\Route\Contracts\RouteServiceContract;
use App\Services\ShipmentImporter\Contracts\ShipmentImporterServiceContract;
use App\Services\ShipmentImporter\Contracts\ShipmentImportHistoryServiceContract;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ShipmentImporterServiceTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    private function getDummyShipment(string $type = null)
    {
        return match ($type) {
            'improper' => json_decode(
                '{"id":4240,"distance":"340579","time":3,"company":{"name":"Ms. Pansy Tremblay"},"carrier":{"name":"Onie Lowe","email":"maida.corwin@gmail.com"},"route":[{"stop_id":250456,"postcode":"19217","city":"Schlagsdorf","country":"DE"},{"stop_id":270605,"postcode":"82418","city":"Riegsee","country":"DE"}]}'
            ),
            default => json_decode(
                '{"id":4240,"distance":340579,"time":3,"company":{"name":"Ms. Pansy Tremblay","email":"tressa.mayert@yahoo.com"},"carrier":{"name":"Onie Lowe","email":"maida.corwin@gmail.com"},"route":[{"stop_id":250456,"postcode":"19217","city":"Schlagsdorf","country":"DE"},{"stop_id":270605,"postcode":"82418","city":"Riegsee","country":"DE"}]}'
            ),
        };
    }

    public function testShipmentImportService_WhenImportIsCalledWithExistingMatchingHistoryRecord_ItReturnsNull()
    {
        $historyServiceMock = $this->mock(ShipmentImportHistoryServiceContract::class);
        $historyServiceMock->shouldReceive('findLastByShipmentSourceId')->once()->andReturn(
            (object) ['id' => 1, 'hash' => '3a06ce45966aa12a4be7512c6454b571bf747853']
        );

        $service = $this->app->make(ShipmentImporterServiceContract::class);
        $dummyShipment = $this->getDummyShipment();
        $this->assertNull($service->import($dummyShipment));
    }

    public function testShipmentImportService_WhenImportIsCalledWithoutMatchingHistoryRecord_ItCallsForShipmentHistoryServiceCreate()
    {
        $historyServiceMock = $this->mock(ShipmentImportHistoryServiceContract::class);
        $historyServiceMock->shouldReceive('findLastByShipmentSourceId')->once();
        $historyServiceMock->shouldReceive('create')->once();

        $service = $this->app->make(ShipmentImporterServiceContract::class);
        $dummyShipment = $this->getDummyShipment();
        $service->import($dummyShipment);
    }

    public function testShipmentImportService_WhenImportCompanyIsCalledWithoutExistingRecord_ItCallsForCompanyRepositoryMethods()
    {
        $companyServiceMock = $this->mock(CompanyServiceContract::class);
        $companyServiceMock->shouldReceive('findByEmail')->once();
        $companyServiceMock->shouldReceive('create')->once();

        $service = $this->app->make(ShipmentImporterServiceContract::class);
        $dummyShipment = $this->getDummyShipment();
        $service->importCompany($dummyShipment->company);
    }

    public function testShipmentImportService_WhenImportCarrierIsCalledWithoutExistingRecord_ItCallsForCarrierRepositoryMethods()
    {
        $carrierServiceMock = $this->mock(CarrierServiceContract::class);
        $carrierServiceMock->shouldReceive('findByEmail')->once();
        $carrierServiceMock->shouldReceive('create')->once();

        $service = $this->app->make(ShipmentImporterServiceContract::class);
        $dummyShipment = $this->getDummyShipment();
        $service->importCarrier($dummyShipment->company);
    }

    public function testShipmentImportService_WhenImportCityIsCalledWithoutExistingRecord_ItCallsForCarrierRepositoryMethods()
    {
        $countryServiceMock = $this->mock(CountryServiceContract::class);
        $countryServiceMock->shouldReceive('findByCode')->once()->andReturn(
            (object) ["id" => 1]
        );

        $cityServiceMock = $this->mock(CityServiceContract::class);
        $cityServiceMock->shouldReceive('findByName')->once();
        $cityServiceMock->shouldReceive('create')->once();

        $service = $this->app->make(ShipmentImporterServiceContract::class);
        $service->importCity("dummyCityName", "dummyCountryCode");
    }

    public function testShipmentImportService_WhenImportCityIsCalledWithNotExistingCountry_ItThrowsCountryNotFoundException()
    {
        $this->expectException(CountryNotFoundException::class);
        $countryServiceMock = $this->mock(CountryServiceContract::class);
        $countryServiceMock->shouldReceive('findByCode')->once()->andReturnNull();

        $service = $this->app->make(ShipmentImporterServiceContract::class);
        $service->importCity("dummyCityName", "dummyCountryCode");
    }

    public function testShipmentImportService_WhenImportRouteIsCalledWithoutExistingRecords_ItCallsForRouteAndCityRepositoryMethods()
    {
        $countryServiceMock = $this->mock(CountryServiceContract::class);
        $countryServiceMock->shouldReceive('findByCode')->once()->andReturn(
            (object) ["id" => 1]
        );

        $cityServiceMock = $this->mock(CityServiceContract::class);
        $cityServiceMock->shouldReceive('findByName')->once()->andReturn(
            (object) ["id" => 1]
        );

        $routeServiceMock = $this->mock(RouteServiceContract::class);
        $routeServiceMock->shouldReceive('findBySourceId')->once();
        $routeServiceMock->shouldReceive('create')->once();

        $service = $this->app->make(ShipmentImporterServiceContract::class);
        $dummyInfo = $this->getDummyShipment();

        $service->importRoute($dummyInfo->route[0]);
    }
}
