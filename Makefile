.DEFAULT_GOAL := help
COMPOSE_FILES=docker-compose.yml

.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

env:
	@[ -e ./.env ] || cp -v deploy/docker/develop/app/.env.example ./.env

up:
	if ! [ -f .env ];then cp deploy/docker/develop/app/.env.example .env;fi
	docker-compose -f $(COMPOSE_FILES) up -d
	docker-compose -f $(COMPOSE_FILES) run app composer install
	docker-compose -f $(COMPOSE_FILES) run app php artisan key:generate
	docker-compose -f $(COMPOSE_FILES) run app php artisan migrate
	docker-compose -f $(COMPOSE_FILES) run app php artisan db:seed

build:
	docker-compose -f $(COMPOSE_FILES) up -d --build --force-recreate

down:
	docker-compose -f $(COMPOSE_FILES) down

update:
	docker-compose -f $(COMPOSE_FILES) run app composer install --optimize-autoloader

status:
	docker-compose -f $(COMPOSE_FILES) ps

destroy:
	docker-compose -f $(COMPOSE_FILES) down --remove-orphans

shell:
	docker-compose -f $(COMPOSE_FILES) exec app bash

consume:
	@docker-compose -f $(COMPOSE_FILES) run app php artisan consume:shipments
	@docker-compose -f $(COMPOSE_FILES) run app python3 bin/python/importer/consumer.py

prepare:
	docker-compose -f $(COMPOSE_FILES) exec app php artisan migrate --force
	docker-compose -f $(COMPOSE_FILES) exec app php artisan cache:clear

run-tests:
	docker-compose -f $(COMPOSE_FILES) run app vendor/bin/phpunit
