<?php

return [
    'pagination' => [
        'per_page' => 1,
    ],
    'importer' => [
        'validation' => [
            "json_schema_path" => storage_path('shipment_importer_schema.json')
        ],
        'hash' => [
            'algorithm' => env('SHIPMENT_IMPORTER_HASH_ALGORITHM', 'sha1')
        ]
    ],
    'producer' => [
        'queue' => [
            'routing_key' => 'shipment_service',
            'queue' => 'shipments'
        ]
    ]
];
