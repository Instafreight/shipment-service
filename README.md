# InstaFreight Shipment Service Docs
Please refer to the `docs` directory for full information about the project.

The challenge description can be found in [Senior_Backend_Code_Challenge_API.pdf](docs/assets/Senior_Backend_Code_Challenge_API.pdf).
The dataset provided in the challenge can also be found in [Shipments.json](docs/assets/shipments.json).

Documents include:
- [Architecture](docs/ARCHITECTURE.md): software architecture of the backend app
- [DATABASE](docs/DATABASE.md): Database Schema
- [DEVELOP_SETUP](docs/DEVELOP_SETUP.md): Local Development Setup
- [PIPELINE](docs/PIPELINE.md): Pipeline procedures
- [SECURITY](docs/SECURITY.md): Security measures implemented
- [TECH_STACK](docs/TECH_STACK.md): Technology stack
- Swagger documentation can be accessed at `/api/documentation`
- You can find the [presentation file here](docs/assets/presentation.pptx).


## Local Development
Run the command
```
make up
```
at the root of project then open [localhost:8080](http://localhost:8080). 
It will handle everything necessary to run the project. If you need more flexibility and option
just follow the guides in `doc/DEVELOP_SETUP.md`.

## Live
At the time of this implementation, the app is up on a heroku instance. It can be accessed 
from [instafreight-shipment-service.herokuapp.com](https://instafreight-shipment-service.herokuapp.com/).

**Note**: It has some limitation because of heroku so the consumers are not working. 
So please don't use the `api/v1/shipments/import` which needs consumers to process the shipments, 
instead use the sync version accessible `api/v1/shipments/import-sync`.
