from peewee import *
import os


class Database:
    def __init__(self):
        self.connection = None
        self.driver = os.getenv('DB_CONNECTION', 'mysql')
        self.database = os.getenv('DB_DATABASE', 'instafreight')
        self.host = os.getenv('DB_HOST', '127.0.0.1')
        self.port = os.getenv('DB_PORT', 3307)
        self.user = os.getenv('DB_USER', 'root')
        self.password = os.getenv('DB_PASSWORD', 'root')

        self.connect()

    def connect(self):
        if self.driver == 'mysql':
            self.connection = MySQLDatabase(self.database,
                                            user = self.user,
                                            password = self.password,
                                            host = self.host,
                                            port = self.port)
        if self.driver == 'pgsql':
            self.connection = PostgresqlDatabase(self.database,
                                            user = self.user,
                                            password = self.password,
                                            host = self.host,
                                            port = self.port)

        return self.connection
