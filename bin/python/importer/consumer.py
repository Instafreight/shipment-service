#!/usr/bin/env python
import pika
import functools
import logging
import json
import os


from ShipmentImporter import ShipmentImporter

LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
              '-35s %(lineno) -5d: %(message)s')
LOGGER = logging.getLogger(__name__)

logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)

importer = ShipmentImporter()

def on_message(chan, method_frame, _header_frame, body):
    """Called when a message is received. Log message and ack it."""
    # LOGGER.info('Userdata: %s Message body: %s')
    jsonBody = json.loads(body)
    try:
        importer.doImport(jsonBody)
    except Exception as e:
        LOGGER.critical('Error in importer: %s', e)
    chan.basic_ack(delivery_tag=method_frame.delivery_tag)

def main():
    """Main method."""
    rabbitHost = os.getenv('AMQP_HOST', 'rabbitmq')
    rabbitPort = os.getenv('AMQP_PORT', 5672)
    rabbitUser = os.getenv('AMQP_USER', 'guest')
    rabbitPassword = os.getenv('AMQP_PASSWORD', 'guest')
    rabbitVhost = os.getenv('AMQP_VHOST', '/')
    queue_name = 'shipments'

    credentials = pika.PlainCredentials(rabbitUser, rabbitPassword)
    parameters = pika.ConnectionParameters(rabbitHost, rabbitPort, rabbitVhost, credentials)
    connection = pika.BlockingConnection(parameters)

    channel = connection.channel()
    channel.exchange_declare(exchange="shipments_exchange",
                             exchange_type="direct",
                             passive=False,
                             durable=True,
                             auto_delete=False)
    channel.queue_declare(queue=queue_name, durable=True)
    channel.queue_bind(queue=queue_name, exchange="shipments_exchange", routing_key="shipment_service")
    channel.basic_qos(prefetch_count=1)

    on_message_callback = functools.partial(on_message)
    channel.basic_consume(queue_name, on_message_callback)

    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        channel.stop_consuming()

    connection.close()

if __name__ == '__main__':
    main()
    exit(0)
    # from models.Company import Company
    # from ShipmentImporter import ShipmentImporter
    # import time
    # body = '{"id":4240,"distance":340579,"time":3,"company":{"name":"Ms. Pansy Tremblat","email":"tressa112s63e.mayert@yahoo.com"},"carrier":{"name":"Onie Lowe","email":"maida.corwin@gmail.com"},"route":[{"stop_id":250456,"postcode":"192187","city":"Neustädt","country":"DE"},{"stop_id":270605,"postcode":"82418","city":"Riegsee","country":"DE"}]}'
    # jsonBody = json.loads(body)
    # print(jsonBody['route'][0]['city'])
    # exit(0)
    # importer = ShipmentImporter()
    # start = time.time_ns()
    # importer.doImport(jsonBody)
    # print((time.time_ns() - start)/1000/1000)
    # company = Company(name='mohsen', email='m@n.com')
    # company.save()
