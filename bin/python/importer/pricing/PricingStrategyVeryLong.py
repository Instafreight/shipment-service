from pricing.PriceCalculationStrategy import PriceCalculationStrategy


class PricingStrategyVeryLong(PriceCalculationStrategy):
    def getUnitPrice(self):
        return 0.15
