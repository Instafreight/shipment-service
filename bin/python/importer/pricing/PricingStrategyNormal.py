from pricing.PriceCalculationStrategy import PriceCalculationStrategy


class PricingStrategyNormal(PriceCalculationStrategy):
    def getUnitPrice(self):
        return 0.25
