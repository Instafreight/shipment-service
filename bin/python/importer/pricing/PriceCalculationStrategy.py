
class PriceCalculationStrategy:
    def calculate(self, distance):
        return distance * self.getAdjustment() * self.getUnitPrice()

    def getAdjustment(self):
        return 0.00001

    def getUnitPrice(self):
        return 0.1