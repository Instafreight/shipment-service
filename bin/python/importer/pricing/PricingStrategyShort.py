from pricing.PriceCalculationStrategy import PriceCalculationStrategy


class PricingStrategyShort(PriceCalculationStrategy):
    def getUnitPrice(self):
        return 0.30
