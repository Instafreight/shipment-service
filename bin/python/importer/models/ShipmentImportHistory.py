from peewee import *
from models.BaseModel import BaseModel
from models.Shipment import Shipment


class ShipmentImportHistory(BaseModel):
    hash = CharField()
    algorithm = CharField()
    shipment = ForeignKeyField(Shipment, backref='shipment_import_histories')
    created_at = DateTimeField
    updated_at = DateTimeField

    class Meta:
        db_table = 'shipment_import_histories'
