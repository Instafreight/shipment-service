from peewee import *
from Database import Database

dbObject = Database().connect()


class BaseModel(Model):
    """A base model that will use our database"""

    class Meta:
        database = dbObject
        table_settings = ['ENGINE=InnoDB', 'DEFAULT CHARSET=utf8']
