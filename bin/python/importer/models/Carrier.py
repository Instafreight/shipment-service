from peewee import *
from models.BaseModel import BaseModel


class Carrier(BaseModel):
    name = TextField()
    email = TextField()

    class Meta:
        db_table = 'carriers'
