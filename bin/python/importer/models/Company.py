from peewee import *
from models.BaseModel import BaseModel


class Company(BaseModel):
    name = TextField()
    email = TextField()

    class Meta:
        db_table = 'companies'
