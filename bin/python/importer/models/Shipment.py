from peewee import *
from models.BaseModel import BaseModel
from models.Company import Company
from models.Carrier import Carrier
from models.Route import Route


class Shipment(BaseModel):
    distance = IntegerField()
    time = IntegerField()
    price = FloatField()
    source_id = IntegerField()
    company = ForeignKeyField(Company, backref='shipments')
    carrier = ForeignKeyField(Carrier, backref='shipments')
    start_route = ForeignKeyField(Route, backref='shipments')
    end_route = ForeignKeyField(Route, backref='shipments')

    class Meta:
        db_table = 'shipments'
