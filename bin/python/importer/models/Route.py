from peewee import *
from models.BaseModel import BaseModel
from models.City import City


class Route(BaseModel):
    post_code = TextField()
    source_id = IntegerField()
    city = ForeignKeyField(City, backref='routes')

    class Meta:
        db_table = 'routes'
