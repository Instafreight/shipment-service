from peewee import *
from models.BaseModel import BaseModel
from models.Country import Country


class City(BaseModel):
    name = TextField()
    country = ForeignKeyField(Country, backref='cities')

    class Meta:
        db_table = 'cities'
