from peewee import *
from models.BaseModel import BaseModel


class Country(BaseModel):
    name = TextField()
    alpha2 = TextField()

    class Meta:
        db_table = 'countries'
