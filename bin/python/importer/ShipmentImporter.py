import os
import hashlib
import json
from models.ShipmentImportHistory import ShipmentImportHistory
from models.Shipment import Shipment
from models.Company import Company
from models.Carrier import Carrier
from models.Country import Country
from models.City import City
from models.Route import Route
from pricing.PricingStrategyVeryLong import PricingStrategyVeryLong
from pricing.PricingStrategyLong import PricingStrategyLong
from pricing.PricingStrategyShort import PricingStrategyShort
from pricing.PricingStrategyNormal import PricingStrategyNormal


class ShipmentImporter:
    def doImport(self, shipment):
        if self.checkShipmentImportHistory(shipment):
            return None

        companyEntity = self.createCompany(shipment["company"])
        carrierEntity = self.createCarrier(shipment["carrier"])
        startRouteEntity = self.createRoute(shipment["route"][0])
        endRouteEntity = self.createRoute(shipment["route"][1])
        shipmentEntity = self.createShipment(shipment,
                                             companyEntity,
                                             carrierEntity,
                                             startRouteEntity,
                                             endRouteEntity)

        self.createShipmentImportHistory(shipment, shipmentEntity)

        return shipmentEntity

    def checkShipmentImportHistory(self, shipment):
        shipmentHash = self.hashJson(shipment)
        lastShipmentImportHistory = (ShipmentImportHistory
                                     .select()
                                     .join(Shipment)
                                     .where(Shipment.source_id == shipment['id'])
                                     .order_by(ShipmentImportHistory.id.desc())
                                     .get_or_none())

        return (lastShipmentImportHistory is not None) and lastShipmentImportHistory.hash == shipmentHash

    def createShipmentImportHistory(self, info, shipmentEntity):
        shipmentHash = self.hashJson(info)

        return ShipmentImportHistory.create(hash=shipmentHash, algorithm=self.getHashAlgorithm(), shipment=shipmentEntity)

    def createCompany(self, info):
        company = Company.select().where(Company.email == info['email']).get_or_none()
        if company is None:
            return Company.create(name=info['name'], email=info['email'])

        if company.name != info['name'] or company.email != info['email']:
            company.name = info['name']
            company.email = info['email']
            company.save()

        return company

    def createCarrier(self, info):
        carrier = Carrier.select().where(Carrier.email == info['email']).get_or_none()
        if carrier is None:
            return Carrier.create(name=info['name'], email=info['email'])

        if carrier.name != info['name'] or carrier.email != info['email']:
            carrier.name = info['name']
            carrier.email = info['email']
            carrier.save()

        return carrier

    def createCity(self, cityName, countryCode):
        country = Country.select().where(Country.alpha2 == countryCode).get_or_none()
        city = City.select().where(City.name == cityName).get_or_none()
        if city is None:
            return City.create(name=cityName, country=country)

        if city.name != cityName or city.country_id != country.id:
            city.name = cityName
            city.country = country
            city.save()

        return city

    def createRoute(self, info):
        city = self.createCity(info['city'], info['country'])
        route = Route.select().where(Route.source_id == info['stop_id']).get_or_none()
        if route is None:
            return Route.create(post_code=info['postcode'], source_id=info['stop_id'], city=city)

        if route.post_code != info['postcode'] or route.source_id != info['stop_id']:
            route.post_code = info['postcode']
            route.source_id = info['stop_id']
            route.save()

        return route

    def createShipment(self, info, company, carrier, startRoute, endRoute):
        shipment = Shipment.select().where(Shipment.source_id == info['id']).get_or_none()
        if shipment is None:
            calculatedPrice = self.calculateShipmentPrice(info['distance'])
            return Shipment.create(
                distance = info['distance'],
                time = info['time'],
                price = calculatedPrice,
                source_id = info['id'],
                company = company,
                carrier = carrier,
                start_route = startRoute,
                end_route = endRoute
            )

        return shipment

    def calculateShipmentPrice(self, distance):
        if distance <= 100:
            strategy = PricingStrategyShort()
        elif distance <= 200:
            strategy = PricingStrategyNormal()
        elif distance <= 300:
            strategy = PricingStrategyLong()
        else:
            strategy = PricingStrategyVeryLong()

        return strategy.calculate(distance)

    def getHashAlgorithm(self):
        return os.getenv('SHIPMENT_IMPORTER_HASH_ALGORITHM', 'sha1')

    def hashJson(self, info):
        algorithm = self.getHashAlgorithm()

        return getattr(hashlib, algorithm)(json.dumps(info).encode()).hexdigest()
