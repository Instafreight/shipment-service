<?php

namespace App\Services\Carrier\Repositories;

use App\Services\Base\Repository\Concretes\EloquentRepository;
use App\Services\Carrier\Contracts\CarrierRepositoryContract;
use App\Services\Carrier\Models\Carrier;

class CarrierEloquentRepository extends EloquentRepository implements CarrierRepositoryContract
{
    public function __construct(Carrier $model)
    {
        parent::__construct($model);
    }

    public function findByEmail(string $email, array $columns = ['*'])
    {
        return $this->model->select($columns)->where('email', $email)->first();
    }
}
