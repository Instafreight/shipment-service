<?php

namespace App\Services\Carrier;

use App\Services\Carrier\Concretes\CarrierService;
use App\Services\Carrier\Contracts\CarrierRepositoryContract;
use App\Services\Carrier\Contracts\CarrierServiceContract;
use App\Services\Carrier\Repositories\CarrierEloquentRepository;
use Illuminate\Support\ServiceProvider;

class CarrierServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(CarrierRepositoryContract::class, CarrierEloquentRepository::class);
        $this->app->bind(CarrierServiceContract::class, CarrierService::class);
    }
}
