<?php

namespace App\Services\Carrier\Contracts;

use App\Services\Base\Repository\Contracts\AbstractRepositoryContract;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="Carrier",
 *     description="Carrier Repository",
 *     @OA\Xml(
 *         name="Carrier"
 *     ),
 *     @OA\Property(
 *          property="id",
 *          title="ID",
 *          description="ID of the Carrier",
 *          format="integer"
 *     ),
 *     @OA\Property(
 *          property="name",
 *          title="Name",
 *          description="name of the carrier",
 *          format="string"
 *     ),
 *     @OA\Property(
 *          property="email",
 *          title="Email",
 *          description="email of the carrier",
 *          format="string"
 *     )
 * )
 */
interface CarrierRepositoryContract extends AbstractRepositoryContract
{
    /**
     * Find carrier entity by email.
     *
     * @param string $email
     * @param array $columns
     */
    public function findByEmail(string $email, array $columns = ['*']);
}
