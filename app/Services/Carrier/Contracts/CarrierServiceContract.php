<?php

namespace App\Services\Carrier\Contracts;

interface CarrierServiceContract
{
    public function all();

    public function findById(int $id);

    public function findByEmail(string $email);

    public function create(array $data);

    public function update(int $id, array $data);
}
