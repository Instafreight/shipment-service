<?php

namespace App\Services\Carrier\Concretes;



use App\Services\Base\Repository\Exceptions\EntityCreateException;
use App\Services\Base\Repository\Exceptions\EntityNotFoundException;
use App\Services\Base\Repository\Exceptions\EntityUpdateException;
use App\Services\Base\Validation\Exception\ValidationException;
use App\Services\Base\Validation\Traits\hasValidation;
use App\Services\Carrier\Contracts\CarrierRepositoryContract;
use App\Services\Carrier\Contracts\CarrierServiceContract;
use App\Services\Carrier\Validations\CreateValidator;
use App\Services\Carrier\Validations\UpdateValidator;
use Illuminate\Database\Eloquent\Collection;

class CarrierService implements CarrierServiceContract
{
    use hasValidation;

    private CarrierRepositoryContract $repository;

    public function __construct(CarrierRepositoryContract $carrierRepository)
    {
        $this->repository = $carrierRepository;
    }

    public function all(): Collection
    {
        return $this->repository->all();
    }

    public function findById(int $id)
    {
        return $this->repository->findById($id);
    }

    public function findByEmail(string $email)
    {
        return $this->repository->findByEmail($email);
    }

    /**
     * @throws ValidationException
     * @throws EntityCreateException
     */
    public function create(array $data)
    {
        $this->validate(CreateValidator::class, $data);

        return $this->repository->create($data);
    }

    /**
     * @throws EntityNotFoundException
     * @throws ValidationException
     * @throws EntityUpdateException
     */
    public function update(int $id, array $data)
    {
        $this->validate(UpdateValidator::class, $data);

        return $this->repository->update($id, $data);
    }
}
