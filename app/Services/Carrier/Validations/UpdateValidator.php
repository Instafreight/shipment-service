<?php

namespace App\Services\Carrier\Validations;

use App\Services\Base\Validation\AbstractValidator;
use Illuminate\Validation\Rule;

class UpdateValidator extends AbstractValidator
{
    public function rules(): array
    {
        return [
            "name" => 'string',
            "email" => [
                'email',
                Rule::unique('carriers')->ignore($this->entityId),
            ],
        ];
    }
}
