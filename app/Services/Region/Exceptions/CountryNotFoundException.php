<?php

namespace App\Services\Region\Exceptions;

use App\Services\Base\Exception\AppException;

class CountryNotFoundException extends AppException
{

}
