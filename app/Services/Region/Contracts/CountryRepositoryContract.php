<?php

namespace App\Services\Region\Contracts;

use App\Services\Base\Repository\Contracts\AbstractRepositoryContract;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="Country",
 *     description="Country Repository",
 *     @OA\Xml(
 *         name="Country"
 *     ),
 *     @OA\Property(
 *          property="id",
 *          title="ID",
 *          description="ID of the country",
 *          format="integer"
 *     ),
 *     @OA\Property(
 *          property="name",
 *          title="Name",
 *          description="name of the company",
 *          format="string"
 *     ),
 *     @OA\Property(
 *          property="alpha2",
 *          title="Alpha2",
 *          description="Alpha2 code of the company",
 *          format="string"
 *     )
 * )
 */
interface CountryRepositoryContract extends AbstractRepositoryContract
{
    /**
     * Find city entity by code.
     *
     * @param string $code
     * @param array $columns
     */
    public function findByCode(string $code, array $columns = ['*']);
}
