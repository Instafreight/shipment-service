<?php

namespace App\Services\Region\Contracts;

interface CountryServiceContract
{
    public function all();

    public function findById(int $id);

    public function findByCode(string $code);
}
