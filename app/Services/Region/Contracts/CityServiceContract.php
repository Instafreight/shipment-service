<?php

namespace App\Services\Region\Contracts;

interface CityServiceContract
{
    public function all();

    public function findById(int $id);

    public function findByName(string $name);

    public function create(array $data);

    public function update(int $id, array $data);
}
