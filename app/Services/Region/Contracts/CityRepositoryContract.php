<?php

namespace App\Services\Region\Contracts;

use App\Services\Base\Repository\Contracts\AbstractRepositoryContract;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="City",
 *     description="City Repository",
 *     @OA\Xml(
 *         name="City"
 *     ),
 *     @OA\Property(
 *          property="id",
 *          title="ID",
 *          description="ID of the City",
 *          format="integer"
 *     ),
 *     @OA\Property(
 *          property="name",
 *          title="Name",
 *          description="name of the City",
 *          format="string"
 *     ),
 *     @OA\Property(
 *          property="country_id",
 *          title="Country ID",
 *          description="ID of the country",
 *          format="integer"
 *     ),
 *     @OA\Property(
 *          property="country",
 *          title="Country",
 *          description="Country relation of the model",
 *          ref="#/components/schemas/CountryRepositoryContract"
 *     )
 * )
 */
interface CityRepositoryContract extends AbstractRepositoryContract
{
    /**
     * Find city entity by name.
     *
     * @param string $name
     * @param array $columns
     */
    public function findByName(string $name, array $columns = ['*']);
}
