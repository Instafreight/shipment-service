<?php

namespace App\Services\Region\Concretes;

use App\Services\Base\Repository\Exceptions\EntityCreateException;
use App\Services\Base\Repository\Exceptions\EntityNotFoundException;
use App\Services\Base\Repository\Exceptions\EntityUpdateException;
use App\Services\Base\Validation\Exception\ValidationException;
use App\Services\Base\Validation\Traits\hasValidation;
use App\Services\Region\Contracts\CityRepositoryContract;
use App\Services\Region\Contracts\CityServiceContract;
use App\Services\Region\Validations\City\CreateValidator;
use App\Services\Region\Validations\City\UpdateValidator;
use Illuminate\Database\Eloquent\Collection;

class CityService implements CityServiceContract
{
    use hasValidation;

    private CityRepositoryContract $repository;

    public function __construct(CityRepositoryContract $cityRepository)
    {
        $this->repository = $cityRepository;
    }

    public function all(): Collection
    {
        return $this->repository->all();
    }

    public function findById(int $id)
    {
        return $this->repository->findById($id);
    }

    public function findByName(string $name)
    {
        return $this->repository->findByName($name);
    }

    /**
     * @throws ValidationException
     * @throws EntityCreateException
     */
    public function create(array $data)
    {
//        $this->validate(CreateValidator::class, $data);

        return $this->repository->create($data);
    }

    /**
     * @throws ValidationException
     * @throws EntityNotFoundException
     * @throws EntityUpdateException
     */
    public function update(int $id, array $data)
    {
//        $this->validate(UpdateValidator::class, $data);

        return $this->repository->update($id, $data);
    }
}
