<?php

namespace App\Services\Region\Concretes;

use App\Services\Region\Contracts\CountryRepositoryContract;
use App\Services\Region\Contracts\CountryServiceContract;
use Illuminate\Database\Eloquent\Collection;

class CountryService implements CountryServiceContract
{
    private CountryRepositoryContract $repository;

    public function __construct(CountryRepositoryContract $countryRepository)
    {
        $this->repository = $countryRepository;
    }

    public function all(): Collection
    {
        return $this->repository->all();
    }

    public function findById(int $id)
    {
        return $this->repository->findById($id);
    }

    public function findByCode(string $code)
    {
        return $this->repository->findByCode($code);
    }
}
