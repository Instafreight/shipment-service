<?php

namespace App\Services\Region\Validations\City;

use App\Services\Base\Validation\AbstractValidator;
use Illuminate\Validation\Rule;

class UpdateValidator extends AbstractValidator
{
    public function rules(): array
    {
        return [
            "name" => [
                'string',
                Rule::unique('cities')->ignore($this->entityId),
            ],
            "country_id" => 'integer|exists:countries,id',
        ];
    }
}
