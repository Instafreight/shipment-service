<?php

namespace App\Services\Region\Validations\City;

use App\Services\Base\Validation\AbstractValidator;

class CreateValidator extends AbstractValidator
{
    public function rules(): array
    {
        return [
            "name" => 'required|string|unique:cities,name',
            "country_id" => 'required|integer|exists:countries,id',
        ];
    }
}
