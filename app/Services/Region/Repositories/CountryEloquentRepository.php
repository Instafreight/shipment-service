<?php

namespace App\Services\Region\Repositories;

use App\Services\Base\Repository\Concretes\EloquentRepository;
use App\Services\Region\Contracts\CountryRepositoryContract;
use App\Services\Region\Models\Country;

class CountryEloquentRepository extends EloquentRepository implements CountryRepositoryContract
{
    public function __construct(Country $model)
    {
        parent::__construct($model);
    }

    public function findByCode(string $code, array $columns = ['*'])
    {
        return $this->model->select($columns)->where('alpha2', $code)->first();
    }
}
