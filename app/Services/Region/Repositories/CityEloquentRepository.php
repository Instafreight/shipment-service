<?php

namespace App\Services\Region\Repositories;

use App\Services\Base\Repository\Concretes\EloquentRepository;
use App\Services\Region\Contracts\CityRepositoryContract;
use App\Services\Region\Models\City;

class CityEloquentRepository extends EloquentRepository implements CityRepositoryContract
{
    public function __construct(City $model)
    {
        parent::__construct($model);
    }

    public function findByName(string $name, array $columns = ['*'])
    {
        return $this->model->select($columns)->where('name', $name)->first();
    }
}
