<?php

namespace App\Services\Region;

use App\Services\Region\Concretes\CityService;
use App\Services\Region\Concretes\CountryService;
use App\Services\Region\Contracts\CityRepositoryContract;
use App\Services\Region\Contracts\CityServiceContract;
use App\Services\Region\Contracts\CountryRepositoryContract;
use App\Services\Region\Contracts\CountryServiceContract;
use App\Services\Region\Repositories\CityEloquentRepository;
use App\Services\Region\Repositories\CountryEloquentRepository;
use Illuminate\Support\ServiceProvider;

class RegionServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(CountryRepositoryContract::class, CountryEloquentRepository::class);
        $this->app->bind(CityRepositoryContract::class, CityEloquentRepository::class);
        $this->app->bind(CityServiceContract::class, CityService::class);
        $this->app->bind(CountryServiceContract::class, CountryService::class);
    }
}
