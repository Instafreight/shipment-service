<?php

namespace App\Services\ShipmentImporter\Commands;

use App\Services\Base\Validation\Exception\ValidationException;
use App\Services\Base\Validation\JsonSchemaValidator;
use App\Services\ShipmentImporter\Contracts\ShipmentImporterServiceContract;
use App\Services\ShipmentImporter\Producers\ShipmentRabbitProducer;
use Illuminate\Console\Command;
use Illuminate\Contracts\Container\BindingResolutionException;
use JsonMachine\Exception\InvalidArgumentException;
use JsonMachine\Items;

class ImportShipmentsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:shipments {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Shipments from json file';

    /**
     * Execute the console command.
     *
     * @return void
     * @throws BindingResolutionException
     * @throws InvalidArgumentException
     */
    public function handle(): void
    {
        $filePath = $this->argument('file');
        $shipments = Items::fromFile($filePath);
        $startTimer = microtime(true);
        $shipmentImporterService = app()->make(ShipmentImporterServiceContract::class);

        $jsonValidator = app()->make(JsonSchemaValidator::class);
        $schemaPath = config('shipment.importer.validation.json_schema_path');
        $counter = 0;
        foreach ($shipments as $shipment) {
            $counter++;
//            if($counter > 1) {
//                break;
//            }
            if(!$jsonValidator->validate($schemaPath, $shipment)) {
                $this->line("Invalid Json Schema for shipment ". $shipment->id);
                continue;
            }
            try{
                $shipmentEntity = $shipmentImporterService->import($shipment);
//                (new ShipmentRabbitProducer())->dispatch($shipment);
            } catch (ValidationException $e) {
                continue;
            }
        }

        $this->info("Import Finished in ". round((microtime(true) - $startTimer) * 1000, 2) . " ms");
    }
}
