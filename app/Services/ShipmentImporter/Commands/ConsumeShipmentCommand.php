<?php

namespace App\Services\ShipmentImporter\Commands;

use App\Services\Base\Validation\Exception\ValidationException;
use App\Services\Base\Validation\JsonSchemaValidator;
use App\Services\ShipmentImporter\Contracts\ShipmentImporterServiceContract;
use Bschmitt\Amqp\Facades\Amqp;
use Illuminate\Console\Command;
use Illuminate\Contracts\Container\BindingResolutionException;

class ConsumeShipmentCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'consume:shipments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Consume shipments messages from rabbitmq';

    /**
     * Execute the console command.
     *
     * @return void
     * @throws BindingResolutionException
     */
    public function handle(): void
    {
        // todo: add logging
        $shipmentImporterService = app()->make(ShipmentImporterServiceContract::class);
        $jsonValidator = app()->make(JsonSchemaValidator::class);
        $schemaPath = config('shipment.importer.validation.json_schema_path');

        Amqp::consume('shipments', function ($message, $resolver) use ($shipmentImporterService, $jsonValidator, $schemaPath){
            $shipment = json_decode($message->body);

            try {
                if(!$jsonValidator->validate($schemaPath, $shipment)) {
                    // todo: log
                    $resolver->acknowledge($message);
                    return;
                }
                $shipmentImporterService->import($shipment);
            } catch (ValidationException $e) {
                // todo: log here
                $resolver->acknowledge($message);
                return;
            }

            $resolver->acknowledge($message);
        }, [
            "persistent" => true,
            'qos' => true,
            'qos_prefetch_count' => 1
        ]);
    }
}
