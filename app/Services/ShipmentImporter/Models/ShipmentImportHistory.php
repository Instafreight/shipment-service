<?php

namespace App\Services\ShipmentImporter\Models;

use App\Services\Shipment\Models\Shipment;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ShipmentImportHistory extends Model
{
    use HasFactory;

    protected $fillable = ['shipment_id', 'hash', 'algorithm'];

    public function shipment(): BelongsTo
    {
        return $this->belongsTo(Shipment::class);
    }
}
