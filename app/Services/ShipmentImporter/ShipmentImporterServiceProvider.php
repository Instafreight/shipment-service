<?php

namespace App\Services\ShipmentImporter;

use App\Services\ShipmentImporter\Commands\ConsumeShipmentCommand;
use App\Services\ShipmentImporter\Commands\ImportShipmentsCommand;
use App\Services\ShipmentImporter\Concretes\ShipmentImporterService;
use App\Services\ShipmentImporter\Concretes\ShipmentImportHistoryService;
use App\Services\ShipmentImporter\Contracts\ShipmentImporterServiceContract;
use App\Services\ShipmentImporter\Contracts\ShipmentImportHistoryRepositoryContract;
use App\Services\ShipmentImporter\Contracts\ShipmentImportHistoryServiceContract;
use App\Services\ShipmentImporter\Contracts\ShipmentProducerContract;
use App\Services\ShipmentImporter\Producers\ShipmentRabbitProducer;
use App\Services\ShipmentImporter\Repositories\ShipmentImportHistoryEloquentRepository;
use Illuminate\Support\ServiceProvider;

class ShipmentImporterServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(ShipmentImportHistoryRepositoryContract::class, ShipmentImportHistoryEloquentRepository::class);
        $this->app->bind(ShipmentImportHistoryServiceContract::class, ShipmentImportHistoryService::class);
        $this->app->bind(ShipmentImporterServiceContract::class, ShipmentImporterService::class);
        $this->app->bind(ShipmentProducerContract::class, ShipmentRabbitProducer::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                ImportShipmentsCommand::class,
                ConsumeShipmentCommand::class,
            ]);
        }
    }
}
