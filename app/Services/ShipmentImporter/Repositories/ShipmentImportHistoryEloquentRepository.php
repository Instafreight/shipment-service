<?php

namespace App\Services\ShipmentImporter\Repositories;

use App\Services\Base\Repository\Concretes\EloquentRepository;
use App\Services\ShipmentImporter\Contracts\ShipmentImportHistoryRepositoryContract;
use App\Services\ShipmentImporter\Models\ShipmentImportHistory;

class ShipmentImportHistoryEloquentRepository extends EloquentRepository implements ShipmentImportHistoryRepositoryContract
{
    public function __construct(ShipmentImportHistory $model)
    {
        parent::__construct($model);
    }

    public function findLastByShipmentSourceId(int $shipmentSourceId, array $columns = ['*'])
    {
        return $this->model->select($columns)->whereHas('shipment', function($q) use ($shipmentSourceId) {
            $q->where('source_id', $shipmentSourceId);
        })->latest('id')->first();
    }
}
