<?php

namespace App\Services\ShipmentImporter\Helpers;

class ArrayHelper
{
    public static function isDifferent(array $first, array $second): bool
    {
        return !empty(array_diff($first, $second));
    }
}
