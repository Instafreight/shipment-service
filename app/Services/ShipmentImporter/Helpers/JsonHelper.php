<?php

namespace App\Services\ShipmentImporter\Helpers;

use App\Services\Base\Exception\AppException;
use ValueError;

class JsonHelper
{
    public static function hash($json, $algorithm): string
    {
        if(!is_string($json)) {
            try {
                $json = json_encode($json);
            } catch (\Exception $e) {
                throw new AppException("Json could not be encoded to string.");
            }
        }

        try {
            $cipher = hash($algorithm, $json);
        } catch (ValueError $e) {
            throw new AppException($e->getMessage());
        }

        return $cipher;
    }
}
