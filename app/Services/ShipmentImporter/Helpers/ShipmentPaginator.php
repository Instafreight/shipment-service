<?php

namespace App\Services\ShipmentImporter\Helpers;
use OpenApi\Annotations as OA;

/**
 * @OA\Response(
 *     response="ShipmentPaginator",
 *     description="Shipment Paginator",
 *     @OA\JsonContent(
 *     @OA\Property(
 *          property="current_page",
 *          title="Current Page",
 *          description="Number of the current page",
 *          format="integer"
 *     ),
 *     @OA\Property(
 *          property="data",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/ShipmentRepositoryContract")
 *     ),
 *     @OA\Property(
 *          property="first_page_url",
 *          title="First Page Url",
 *          description="Url of the first page",
 *          format="string"
 *     ),
 *     @OA\Property(
 *          property="from",
 *          title="From",
 *          description="Index of the first element",
 *          format="integer"
 *     ),
 *     @OA\Property(
 *          property="last_page",
 *          title="Last Page",
 *          description="Number of the last page",
 *          format="integer"
 *     ),
 *     @OA\Property(
 *          property="last_page_url",
 *          title="Last Page Url",
 *          description="Url of the last page",
 *          format="string"
 *     ),
 *     @OA\Property(
 *          property="next_page_url",
 *          title="Next Page Url",
 *          description="Url of the Next page",
 *          format="string"
 *     ),
 *     @OA\Property(
 *          property="path",
 *          title="Path",
 *          description="Path",
 *          format="string"
 *     ),
 *     @OA\Property(
 *          property="per_page",
 *          title="Per page",
 *          description="Per page",
 *          format="integer"
 *     ),
 *     @OA\Property(
 *          property="prev_page_url",
 *          title="Previous Page Url",
 *          description="Url of the Previous page",
 *          format="string"
 *     ),
 *     @OA\Property(
 *          property="to",
 *          title="To",
 *          description="Index of the last element",
 *          format="integer"
 *     ),
 *     @OA\Property(
 *          property="total",
 *          title="Total",
 *          description="Total number of elements",
 *          format="integer"
 *     ),
 *     )
 * )
 */
interface ShipmentPaginator
{

}
