<?php

namespace App\Services\ShipmentImporter\Producers;

use App\Services\ShipmentImporter\Contracts\ShipmentProducerContract;
use Bschmitt\Amqp\Facades\Amqp;

class ShipmentRabbitProducer implements ShipmentProducerContract
{
    public function dispatch($shipment): void
    {
        Amqp::publish(
            config('shipment.producer.queue.routing_key'),
            json_encode($shipment),
            ['queue' => config('shipment.producer.queue.queue')]
        );
    }
}
