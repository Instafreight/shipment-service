<?php

namespace App\Services\ShipmentImporter\Contracts;

use App\Services\Base\Repository\Contracts\AbstractRepositoryContract;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="Shipment Import History",
 *     description="Shipment Import History Repository",
 *     @OA\Xml(
 *         name="Shipment Import History"
 *     ),
 *     @OA\Property(
 *          property="id",
 *          title="ID",
 *          description="ID of the Shipment Import History",
 *          format="integer"
 *     ),
 *     @OA\Property(
 *          property="hash",
 *          title="Hash",
 *          description="Hashed value of last change detection",
 *          format="string"
 *     ),
 *     @OA\Property(
 *          property="algorithm",
 *          title="Algorithm",
 *          description="Algorithm used for hashing",
 *          format="string"
 *     ),
 *     @OA\Property(
 *          property="shipment_id",
 *          title="Shipment ID",
 *          description="ID of the shipment",
 *          format="integer"
 *     ),
 *     @OA\Property(
 *          property="created_at",
 *          title="Created at",
 *          description="Creation time of the shipment import history",
 *          format="datetime"
 *     ),
 *     @OA\Property(
 *          property="updated_at",
 *          title="Updated at",
 *          description="Update time of the shipment import history",
 *          format="datetime"
 *     ),
 *     @OA\Property(
 *          property="shipment",
 *          title="Shipment",
 *          description="Shipment relation of the model",
 *          ref="#/components/schemas/ShipmentRepositoryContract"
 *     ),
 * )
 */
interface ShipmentImportHistoryRepositoryContract extends AbstractRepositoryContract
{
    /**
     * Find model by id.
     *
     * @param int $shipmentSourceId
     * @param array $columns
     */
    public function findLastByShipmentSourceId(int $shipmentSourceId, array $columns = ['*']);
}
