<?php

namespace App\Services\ShipmentImporter\Contracts;

interface ShipmentImportHistoryServiceContract
{
    public function findLastByShipmentSourceId(int $shipmentSourceId);

    public function create(array $data);
}
