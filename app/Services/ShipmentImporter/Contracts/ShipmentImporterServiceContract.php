<?php

namespace App\Services\ShipmentImporter\Contracts;

interface ShipmentImporterServiceContract
{
    public function import(object $shipment);

    public function importCompany(object $shipment);

    public function importCarrier(object $shipment);

    public function importRoute(object $shipment);

    public function importShipment(object $info, object $company, object $carrier, object $startRoute, object $endRoute);
}
