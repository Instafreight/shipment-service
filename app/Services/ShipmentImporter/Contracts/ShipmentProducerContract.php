<?php

namespace App\Services\ShipmentImporter\Contracts;

interface ShipmentProducerContract
{
    public function dispatch($shipment): void;
}
