<?php

namespace App\Services\ShipmentImporter\Concretes;

use App\Services\ShipmentImporter\Contracts\ShipmentImportHistoryRepositoryContract;
use App\Services\ShipmentImporter\Contracts\ShipmentImportHistoryServiceContract;

class ShipmentImportHistoryService implements ShipmentImportHistoryServiceContract
{
    private ShipmentImportHistoryRepositoryContract $repository;

    public function __construct(ShipmentImportHistoryRepositoryContract $shipmentImportHistoryRepository)
    {
        $this->repository = $shipmentImportHistoryRepository;
    }

    public function findLastByShipmentSourceId(int $shipmentSourceId)
    {
        return $this->repository->findLastByShipmentSourceId($shipmentSourceId);
    }

    public function create(array $data)
    {
        return $this->repository->create($data);
    }
}
