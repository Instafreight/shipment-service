<?php

namespace App\Services\ShipmentImporter\Concretes;

use App\Services\Carrier\Contracts\CarrierServiceContract;
use App\Services\Company\Contracts\CompanyServiceContract;
use App\Services\Region\Contracts\CityServiceContract;
use App\Services\Region\Contracts\CountryServiceContract;
use App\Services\Region\Exceptions\CountryNotFoundException;
use App\Services\Route\Contracts\RouteServiceContract;
use App\Services\ShipmentImporter\Contracts\ShipmentImporterServiceContract;
use App\Services\ShipmentImporter\Contracts\ShipmentImportHistoryServiceContract;
use App\Services\Shipment\Contracts\ShipmentServiceContract;
use App\Services\ShipmentImporter\Helpers\ArrayHelper;
use App\Services\ShipmentImporter\Helpers\JsonHelper;

class ShipmentImporterService implements ShipmentImporterServiceContract
{
    public function __construct(
        private ShipmentImportHistoryServiceContract $importHistoryService,
        private CompanyServiceContract $companyService,
        private CarrierServiceContract $carrierService,
        private RouteServiceContract $routeService,
        private CityServiceContract $cityService,
        private CountryServiceContract $countryService,
        private ShipmentServiceContract $shipmentService
    ){}

    public function import(object $shipment)
    {
        if($this->checkShipmentImportHistory($shipment)) {
            return null;
        }

        $companyEntity = $this->importCompany($shipment->company);
        $carrierEntity = $this->importCarrier($shipment->carrier);
        $startRouteEntity = $this->importRoute($shipment->route[0]);
        $endRouteEntity = $this->importRoute($shipment->route[1]);
        $shipmentEntity = $this->importShipment(
            $shipment,
            $companyEntity,
            $carrierEntity,
            $startRouteEntity,
            $endRouteEntity
        );

        $this->createShipmentImportHistory($shipment, $shipmentEntity);

        return $shipmentEntity;
    }

    public function checkShipmentImportHistory($shipment): bool
    {
        $shipmentHash = $this->hash($shipment);
        $lastShipmentImportHistory = $this->importHistoryService->findLastByShipmentSourceId($shipment->id);

        return isset($lastShipmentImportHistory) && ($lastShipmentImportHistory->hash === $shipmentHash);
    }

    public function createShipmentImportHistory($info, $shipmentEntity)
    {
        $shipmentHash = $this->hash($info);

        return $this->importHistoryService->create([
            'hash' => $shipmentHash,
            'algorithm' => config('shipment.importer.hash.algorithm'),
            'shipment_id' => $shipmentEntity->id
        ]);
    }

    public function importCompany($info)
    {
        $payloadData = [
            "name" => $info->name,
            "email" => $info->email,
        ];

        $company = $this->companyService->findByEmail($info->email);

        return $this->upsertEntity($company, $this->companyService, $payloadData, ['name', 'email']);
    }

    public function importCarrier($info)
    {
        $payloadData = [
            "name" => $info->name,
            "email" => $info->email,
        ];

        $carrier = $this->carrierService->findByEmail($info->email);

        return $this->upsertEntity($carrier, $this->carrierService, $payloadData, ['name', 'email']);
    }

    public function importRoute($info)
    {
        $city = $this->importCity($info->city, $info->country);

        $payloadData = [
            "post_code" => $info->postcode,
            "city_id" => $city->id,
            'source_id' => $info->stop_id,
        ];

        $route = $this->routeService->findBySourceId($info->stop_id);

        return $this->upsertEntity($route, $this->routeService, $payloadData);
    }

    public function importCity(string $cityName, string $countryCode)
    {
        $country = $this->countryService->findByCode($countryCode);

        if(!$country) {
            throw new CountryNotFoundException();
        }

        $payloadData = [
            "name" => $cityName,
            "country_id" => $country->id,
        ];

        $city = $this->cityService->findByName($cityName);

        return $this->upsertEntity($city, $this->cityService, $payloadData, ['name', 'country_id']);
    }

    public function importShipment($info, $company, $carrier, $startRoute, $endRoute)
    {
        $payloadData = [
            'distance' => $info->distance,
            'time' => $info->time,
            'source_id' => $info->id,
            'company_id' => $company->id,
            'carrier_id' => $carrier->id,
            'start_route_id' => $startRoute->id,
            'end_route_id' => $endRoute->id,
        ];

        $shipment = $this->shipmentService->findBySourceId($info->id);

        return $this->upsertEntity($shipment, $this->shipmentService, $payloadData);
    }

    public function upsertEntity($entity, $service, $payload, $columns = [])
    {
        if($entity) {
            $entityData = collect($entity)->only($columns)->toArray();
            if(ArrayHelper::isDifferent($entityData, $payload)) {
                $entity = $service->update($entity->id, $payload);
            }

            return $entity;
        }

        return $service->create($payload);
    }

    private function hash($json)
    {
        $algorithm = config('shipment.importer.hash.algorithm');

        return JsonHelper::hash($json, $algorithm);
    }
}
