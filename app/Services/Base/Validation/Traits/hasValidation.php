<?php

namespace App\Services\Base\Validation\Traits;

use App\Services\Base\Validation\Exception\ValidationException;

/**
 * Used inside repositories when validation needed.
 */
trait hasValidation
{
    /**
     * Validate the given data using the specified validator class.
     *
     * @param string $validatorClass
     * @param array $data
     * @return bool
     * @throws ValidationException
     */
    public function validate(string $validatorClass, array $data): bool
    {
        $validator = app()->make($validatorClass);
        if(!$validator->validate($data)) {
            throw new ValidationException($validator);
        }

        return true;
    }
}
