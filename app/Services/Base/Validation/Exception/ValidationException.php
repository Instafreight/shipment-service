<?php

namespace App\Services\Base\Validation\Exception;

use App\Services\Base\Exception\AppException;
use App\Services\Base\Validation\AbstractValidator;
use Illuminate\Support\MessageBag;
use Throwable;

/**
 * Custom validation exception class.
 */
class ValidationException extends AppException
{
    /**
     * @var AbstractValidator
     */
    protected AbstractValidator $validator;

    /**
     * @param AbstractValidator $validator
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(AbstractValidator $validator, string $message = "", int $code = 0, ?Throwable $previous = null)
    {
        $this->setValidation($validator);
        parent::__construct($message, $code, $previous);
    }

    /**
     * @param AbstractValidator $validator
     * @return self
     */
    protected function setValidation(AbstractValidator $validator): self
    {
        $this->validator = $validator;

        return $this;
    }

    /**
     * Get the list of exception errors.
     *
     * @return MessageBag
     */
    public function errors(): MessageBag
    {
        return $this->validator->errors();
    }
}
