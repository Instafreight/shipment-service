<?php

namespace App\Services\Base\Validation;

use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Validation\Validator as ValidatorContract;
use Illuminate\Support\MessageBag;

/**
 * Validator class using laravel internal validation.
 */
abstract class AbstractValidator
{
    /**
     * @var ValidatorContract
     */
    protected ValidatorContract $validator;

    /**
     * @var int
     */
    protected int $entityId;

    /**
     * Validate the given payload against the validator rules.
     *
     * @param array $payload
     * @return bool
     */
    public function validate(array $payload): bool
    {
        $this->validator = Validator::make($payload, $this->rules());

        return !$this->validator->fails();
    }

    /**
     * Return list of validation errors.
     *
     * @return MessageBag
     */
    public function errors(): MessageBag
    {
        return $this->validator->errors();
    }

    /**
     * Set the entityId property.
     *
     * @param int $id
     * @return self
     */
    public function setEntityId(int $id): self
    {
        $this->entityId = $id;

        return $this;
    }

    /**
     * Get the list of validator rules.
     *
     * @return array
     */
    abstract public function rules(): array;
}
