<?php

namespace App\Services\Base\Validation;

use JsonSchema\Validator;

/**
 * Used for handling json schema validation.
 */
class JsonSchemaValidator
{
    /**
     * Validate a given data against an specific schema.
     *
     * @param string $schemaPath
     * @param object|array $data
     * @return bool
     */
    public function validate(string $schemaPath, object|array $data): bool
    {
        $validator = new Validator();
        $validator->validate($data, (object)['$ref' => 'file://' . $schemaPath]);

        return $validator->isValid();
    }
}
