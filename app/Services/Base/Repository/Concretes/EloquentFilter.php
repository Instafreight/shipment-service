<?php

namespace App\Services\Base\Repository\Concretes;

use App\Services\Base\Repository\Contracts\CriteriaFilter;

class EloquentFilter implements CriteriaFilter
{
    public function __construct(
        protected string $field,
        protected string $value,
        protected string $operator = '='
    ){}

    public function filter($query): mixed
    {
        return $query->whereHas($this->getRelation(), function($q) {
            $q->where($this->field, $this->operator, $this->value);
        });
    }

    protected function getRelation(): string
    {
        return '';
    }
}
