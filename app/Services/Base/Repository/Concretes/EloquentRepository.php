<?php

namespace App\Services\Base\Repository\Concretes;

use App\Services\Base\Repository\Contracts\AbstractRepositoryContract;
use App\Services\Base\Repository\Exceptions\EntityCreateException;
use App\Services\Base\Repository\Exceptions\EntityDeleteException;
use App\Services\Base\Repository\Exceptions\EntityNotFoundException;
use App\Services\Base\Repository\Exceptions\EntityUpdateException;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * EloquentRepository.
 */
class EloquentRepository implements AbstractRepositoryContract
{
    /**
     * @var Model $model
     */
    protected Model $model;

    /**
     * @var Builder $query
     */
    protected Builder $query;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Get the database query builder.
     *
     * @return Builder
     */
    protected function getQuery(): Builder
    {
        return $this->query ?? $this->model->newQuery();
    }

    /**
     * Set the database query builder.
     *
     * @param Builder $query
     * @return self
     */
    protected function setQuery(Builder $query): self
    {
        $this->query = $query;

        return $this;
    }

    public function all(array $columns = ['*']): Collection
    {
        return $this->getQuery()->get($columns);
    }

    public function findById(int $entityId, array $columns = ['*']): ?Model
    {
        return $this->getQuery()->select($columns)->find($entityId);
    }

    public function findByIdOrFail(int $entityId, array $columns = ['*']): Model
    {
        try {
            return $this->getQuery()->select($columns)->findOrFail($entityId);
        } catch (ModelNotFoundException $e) {
            throw new EntityNotFoundException($this);
        }
    }

    public function filter(CriteriaCollection $criteria = null): self
    {
        $query = $this->getQuery();

        $criteria?->each(function ($criterion) use ($query) {
            $criterion->filter($query);
        });
        $this->setQuery($query);

        return $this;
    }

    public function paginate($limit = null, $offset = null, $columns = ['*'], $pageName = 'page'): LengthAwarePaginator
    {
        return $this->getQuery()->paginate($limit, $columns, $pageName, $offset)->setPath('')->withQueryString();
    }

    public function create(array $payload)
    {
        try{
            $model = $this->model->create($payload);

            return $model->fresh();
        } catch (Exception $e) {
            throw new EntityCreateException($this);
        }
    }

    public function update(int $entityId, array $payload)
    {
        try {
            $model = $this->findByIdOrFail($entityId);

            if($model->update($payload)) {
                return $model->fresh();
            }
        } catch (Exception $e) {
            throw new EntityUpdateException($this);
        }
    }

    public function deleteById(int $modelId): bool
    {
        try {
            $model = $this->findByIdOrFail($modelId);

            return $model->delete();
        } catch (Exception $e) {
            throw new EntityDeleteException($this);
        }
    }

    public function enableEagerLoadRelations(): self
    {
        $relations = $this->getRelations();

        $this->setQuery(
            $this->getQuery()->with($relations)
        );

        return $this;
    }

    public function disableEagerLoadRelations(): self
    {
        $this->setQuery(
            $this->getQuery()->setEagerLoads([])
        );

        return $this;
    }

    public function getRelations(): array
    {
        return [];
    }

    public function toArray(): array
    {
        return $this->model->toArray();
    }
}
