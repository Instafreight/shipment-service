<?php

namespace App\Services\Base\Repository\Concretes;

use Illuminate\Support\Collection;

/**
 * CriteriaCollection.
 * Just a wrapper around the native Collection.
 */
class CriteriaCollection extends Collection
{

}
