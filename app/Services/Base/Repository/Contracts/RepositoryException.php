<?php

namespace App\Services\Base\Repository\Contracts;

use App\Services\Base\Exception\AppException;
use Throwable;

/**
 * RepositoryException.
 * General repository exception.
 */
class RepositoryException extends AppException
{
    /**
     * @var AbstractRepositoryContract
     */
    private AbstractRepositoryContract $repository;

    /**
     * @param AbstractRepositoryContract $repository
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(AbstractRepositoryContract $repository, string $message = "", int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    /**
     * @param AbstractRepositoryContract $repository
     * @return $this
     */
    protected function setRepository(AbstractRepositoryContract $repository): self
    {
        $this->repository = $repository;

        return $this;
    }

    /**
     * @return AbstractRepositoryContract
     */
    public function getRepository(): AbstractRepositoryContract
    {
        return $this->repository;
    }
}
