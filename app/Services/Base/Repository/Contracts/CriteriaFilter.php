<?php

namespace App\Services\Base\Repository\Contracts;

/**
 * CriteriaFilter.
 * Abstraction of filters that can be applied on repositories.
 */
interface CriteriaFilter
{
    /**
     * Apply filter on the query builder.
     *
     * @param $query
     * @return mixed
     */
    public function filter($query): mixed;
}
