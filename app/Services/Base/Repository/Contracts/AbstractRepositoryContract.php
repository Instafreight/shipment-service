<?php

namespace App\Services\Base\Repository\Contracts;

use App\Services\Base\Repository\Concretes\CriteriaCollection;
use App\Services\Base\Repository\Exceptions\EntityCreateException;
use App\Services\Base\Repository\Exceptions\EntityDeleteException;
use App\Services\Base\Repository\Exceptions\EntityNotFoundException;
use App\Services\Base\Repository\Exceptions\EntityUpdateException;
use Illuminate\Database\Eloquent\Collection;

/**
 * AbstractRepositoryContract.
 */
interface AbstractRepositoryContract
{
    /**
     * Get all entities.
     *
     * @param array $columns
     * @return Collection
     */
    public function all(array $columns = ['*']): Collection;

    /**
     * Find entity by id.
     *
     * @param int $entityId
     * @param array $columns
     */
    public function findById(int $entityId, array $columns = ['*']);

    /**
     * Find entity by id or fail.
     *
     * @param int $entityId
     * @param array $columns
     * @throws EntityNotFoundException
     */
    public function findByIdOrFail(int $entityId, array $columns = ['*']);

    /**
     * Apply filters on the query.
     *
     * @param CriteriaCollection|null $criteria
     * @return $this
     */
    public function filter(CriteriaCollection $criteria = null): self;

    /**
     * Get list of paginated entities.
     *
     * @param int|null $limit
     * @param int|null $offset
     * @param array $columns
     * @param string $pageName
     * @return mixed
     */
    public function paginate(int $limit = null, int $offset = null, array $columns = ['*'], string $pageName = 'page'): mixed;

    /**
     * Create an entity.
     *
     * @param array $payload
     * @throws EntityCreateException
     */
    public function create(array $payload);

    /**
     * Update existing entity.
     *
     * @param int $entityId
     * @param array $payload
     * @throws EntityNotFoundException
     * @throws EntityUpdateException
     */
    public function update(int $entityId, array $payload);

    /**
     * Delete entity by id.
     *
     * @param int $entityId
     * @return bool
     * @throws EntityNotFoundException
     * @throws EntityDeleteException
     */
    public function deleteById(int $entityId): bool;

    /**
     * Get entity relation names.
     *
     * @return array
     */
    public function getRelations(): array;

    /**
     * Enable entity's relations eager loading.
     *
     * @return $this
     */
    public function enableEagerLoadRelations(): self;

    /**
     * Disable entity's relations eager loading.
     *
     * @return $this
     */
    public function disableEagerLoadRelations(): self;

    /**
     * Convert the entity to an array.
     *
     * @return array
     */
    public function toArray(): array;
}
