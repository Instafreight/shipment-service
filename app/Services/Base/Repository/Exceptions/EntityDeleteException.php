<?php

namespace App\Services\Base\Repository\Exceptions;

use App\Services\Base\Repository\Contracts\RepositoryException;

class EntityDeleteException extends RepositoryException
{

}
