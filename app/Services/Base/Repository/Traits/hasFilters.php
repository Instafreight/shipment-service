<?php

namespace App\Services\Base\Repository\Traits;

use App\Services\Base\Repository\Concretes\CriteriaCollection;
use Illuminate\Support\Str;

/**
 * Used when repositories have filtering options.
 */
trait hasFilters
{
    /**
     * Get the list of filter request param to class maps.
     *
     * @return array
     */
    abstract public function getFilterMaps(): array;

    /**
     * Extract list of criteria from request filters that can be applied on the repository.
     *
     * @param array $filters
     * @return CriteriaCollection
     */
    protected function extractFilters(array $filters): CriteriaCollection
    {
        $filterMaps = $this->getFilterMaps();
        $filterRegex = '/('.implode('|', array_keys($filterMaps)).')_(.*)/m';

        $criteria = new CriteriaCollection();
        foreach($filters as $requestVar => $val) {
            if(!Str::startsWith($requestVar, array_keys($filterMaps))) {
                continue;
            }
            preg_match_all($filterRegex, $requestVar, $matches);
            $filterMatches = $matches[0];
            $filterObject = new $filterMaps[$filterMatches[1]]($filterMatches[2], $val);

            $criteria->push($filterObject);
        }

        return $criteria;
    }
}
