<?php

namespace App\Services\Base;

use App\Services\Base\Repository\Concretes\EloquentRepository;
use App\Services\Base\Repository\Contracts\EloquentRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class BaseServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(EloquentRepositoryInterface::class, EloquentRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
