<?php

namespace App\Services\Route;

use App\Services\Region\Contracts\CityRepositoryContract;
use App\Services\Region\Contracts\CountryRepositoryContract;
use App\Services\Region\Repositories\CityEloquentRepository;
use App\Services\Region\Repositories\CountryEloquentRepository;
use App\Services\Route\Concretes\RouteService;
use App\Services\Route\Contracts\RouteRepositoryContract;
use App\Services\Route\Contracts\RouteServiceContract;
use App\Services\Route\Repositories\RouteEloquentRepository;
use Illuminate\Support\ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(RouteRepositoryContract::class, RouteEloquentRepository::class);
        $this->app->bind(RouteServiceContract::class, RouteService::class);
    }
}
