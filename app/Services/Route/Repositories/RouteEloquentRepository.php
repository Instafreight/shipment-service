<?php

namespace App\Services\Route\Repositories;

use App\Services\Base\Repository\Concretes\EloquentRepository;
use App\Services\Route\Contracts\RouteRepositoryContract;
use App\Services\Route\Models\Route;

class RouteEloquentRepository extends EloquentRepository implements RouteRepositoryContract
{
    public function __construct(Route $model)
    {
        parent::__construct($model);
    }

    public function findByPostCode(string $postCode, array $columns = ['*'])
    {
        return $this->model->select($columns)->where('post_code', $postCode)->first();
    }

    public function findBySourceId(int $sourceId, array $columns = ['*'])
    {
        return $this->model->select($columns)->where('source_id', $sourceId)->first();
    }
}
