<?php

namespace App\Services\Route\Contracts;

interface RouteServiceContract
{
    public function all();

    public function findById(int $id);

    public function findBySourceId(int $sourceId);

    public function findByPostCode(string $postCode);

    public function create(array $data);

    public function update(int $id, array $data);
}
