<?php

namespace App\Services\Route\Contracts;

use App\Services\Base\Repository\Contracts\AbstractRepositoryContract;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="Route",
 *     description="Route Repository",
 *     @OA\Xml(
 *         name="Route"
 *     ),
 *     @OA\Property(
 *          property="id",
 *          title="ID",
 *          description="ID of the Route",
 *          format="integer"
 *     ),
 *     @OA\Property(
 *          property="post_code",
 *          title="Post Code",
 *          description="Post Code of the City",
 *          format="string"
 *     ),
 *     @OA\Property(
 *          property="city_id",
 *          title="City ID",
 *          description="ID of the city",
 *          format="integer"
 *     ),
 *     @OA\Property(
 *          property="city",
 *          title="City",
 *          description="City relation of the model",
 *          ref="#/components/schemas/CityRepositoryContract"
 *     )
 * )
 */
interface RouteRepositoryContract extends AbstractRepositoryContract
{
    /**
     * Find route entity by post code.
     *
     * @param string $postCode
     * @param array $columns
     */
    public function findByPostCode(string $postCode, array $columns = ['*']);

    /**
     * Find route entity by source id.
     *
     * @param int $sourceId
     * @param array $columns
     */
    public function findBySourceId(int $sourceId, array $columns = ['*']);
}
