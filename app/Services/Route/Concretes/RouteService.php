<?php

namespace App\Services\Route\Concretes;

use App\Services\Base\Repository\Exceptions\EntityCreateException;
use App\Services\Base\Repository\Exceptions\EntityNotFoundException;
use App\Services\Base\Repository\Exceptions\EntityUpdateException;
use App\Services\Base\Validation\Exception\ValidationException;
use App\Services\Base\Validation\Traits\hasValidation;
use App\Services\Route\Contracts\RouteRepositoryContract;
use App\Services\Route\Contracts\RouteServiceContract;
use App\Services\Route\Validations\CreateValidator;
use App\Services\Route\Validations\UpdateValidator;
use Illuminate\Database\Eloquent\Collection;

class RouteService implements RouteServiceContract
{
    use hasValidation;

    private RouteRepositoryContract $repository;

    public function __construct(RouteRepositoryContract $routeRepository)
    {
        $this->repository = $routeRepository;
    }

    public function all(): Collection
    {
        return $this->repository->all();
    }

    public function findById(int $id)
    {
        return $this->repository->findById($id);
    }

    public function findBySourceId(int $sourceId)
    {
        return $this->repository->findBySourceId($sourceId);
    }

    public function findByPostCode(string $postCode)
    {
        return $this->repository->findByPostCode($postCode);
    }

    /**
     * @throws ValidationException
     * @throws EntityCreateException
     */
    public function create(array $data)
    {
//        $this->validate(CreateValidator::class, $data);

        return $this->repository->create($data);
    }

    /**
     * @throws ValidationException
     * @throws EntityNotFoundException
     * @throws EntityUpdateException
     */
    public function update(int $id, array $data)
    {
//        $this->validate(UpdateValidator::class, $data);

        return $this->repository->update($id, $data);
    }
}
