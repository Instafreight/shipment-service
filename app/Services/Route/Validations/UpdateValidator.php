<?php

namespace App\Services\Route\Validations;

use App\Services\Base\Validation\AbstractValidator;
use Illuminate\Validation\Rule;

class UpdateValidator extends AbstractValidator
{
    public function rules(): array
    {
        return [
            "post_code" => 'string',
            "city_id" => 'integer|exists:cities,id',
        ];
    }
}
