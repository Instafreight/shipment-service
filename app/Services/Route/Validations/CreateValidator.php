<?php

namespace App\Services\Route\Validations;

use App\Services\Base\Validation\AbstractValidator;

class CreateValidator extends AbstractValidator
{
    public function rules(): array
    {
        return [
            "post_code" => 'required|string',
            "city_id" => 'required|integer|exists:cities,id',
        ];
    }
}
