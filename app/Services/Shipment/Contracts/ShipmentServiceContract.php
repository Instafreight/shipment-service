<?php

namespace App\Services\Shipment\Contracts;

use App\Services\Base\Repository\Concretes\CriteriaCollection;

interface ShipmentServiceContract
{
    public function all();

    public function allWithFilter(CriteriaCollection $criteria);

    public function findById(int $id);

    public function findBySourceId(int $sourceId);

    public function create(array $data);

    public function update(int $id, array $data);

    public function paginate($limit = 10, $offset = 1);

    public function paginateWithFilter($limit = 10, $offset = 1, array $filters = []);

    public function calculateShipmentPrice(int $distance): float;

    public function getFilterMaps(): array;
}
