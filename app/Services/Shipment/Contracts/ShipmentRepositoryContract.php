<?php

namespace App\Services\Shipment\Contracts;

use App\Services\Base\Repository\Contracts\AbstractRepositoryContract;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="Shipment",
 *     description="Shipment Repository",
 *     @OA\Xml(
 *         name="Shipment"
 *     ),
 *     @OA\Property(
 *          property="id",
 *          title="ID",
 *          description="ID of the Shipment",
 *          format="integer"
 *     ),
 *     @OA\Property(
 *          property="distance",
 *          title="Distance",
 *          description="Distance between stops in meters",
 *          format="integer"
 *     ),
 *     @OA\Property(
 *          property="time",
 *          title="Time",
 *          description="Time in hours",
 *          format="integer"
 *     ),
 *     @OA\Property(
 *          property="price",
 *          title="Price",
 *          description="Calculated Price of the Shipment",
 *          format="integer"
 *     ),
 *     @OA\Property(
 *          property="company_id",
 *          title="Company ID",
 *          description="ID of the Company",
 *          format="integer"
 *     ),
 *     @OA\Property(
 *          property="carrier_id",
 *          title="Carrier ID",
 *          description="ID of the Carrier",
 *          format="integer"
 *     ),
 *     @OA\Property(
 *          property="start_route_id",
 *          title="Starting Route ID",
 *          description="ID of the starting Route",
 *          format="integer"
 *     ),
 *     @OA\Property(
 *          property="end_route_id",
 *          title="Ending route ID",
 *          description="ID of the ending route",
 *          format="integer"
 *     ),
 *     @OA\Property(
 *          property="company",
 *          title="Company",
 *          description="Company relation of the model",
 *          ref="#/components/schemas/CompanyRepositoryContract"
 *     ),
 *     @OA\Property(
 *          property="carrier",
 *          title="Carrier",
 *          description="Carrier relation of the model",
 *          ref="#/components/schemas/CarrierRepositoryContract"
 *     ),
 *     @OA\Property(
 *          property="startRoute",
 *          title="Start Route",
 *          description="Starting Route relation of the model",
 *          ref="#/components/schemas/RouteRepositoryContract"
 *     ),
 *     @OA\Property(
 *          property="endRoute",
 *          title="End Route",
 *          description="Ending Route relation of the model",
 *          ref="#/components/schemas/RouteRepositoryContract"
 *     )
 * )
 */
interface ShipmentRepositoryContract extends AbstractRepositoryContract
{
    /**
     * Find shipment entity by source id.
     *
     * @param int $sourceId
     */
    public function findBySourceId(int $sourceId);
}
