<?php

namespace App\Services\Shipment\Contracts;

/**
 * Strategy pattern for calculation shipment price.
 */
abstract class PriceCalculationStrategyContract
{
    /**
     * Do the math of the calculation.
     *
     * @param int $distance in meters
     * @return float in euros
     */
    public function calculate(int $distance): float
    {
        return $distance * $this->getAdjustment() * $this->getUnitPrice();
    }

    /**
     * Adjust the input distance for the math. Here we adjust:
     *      1- Meters to Kilometers (division by 1000)
     *      2- Cents to Euro (division by 100)
     * @return float
     */
    public function getAdjustment(): float
    {
        return 0.00001;
    }

    /**
     * Get the unit price for the strategy.
     *
     * @return float
     */
    abstract public function getUnitPrice(): float;
}
