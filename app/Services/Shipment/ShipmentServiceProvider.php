<?php

namespace App\Services\Shipment;

use App\Services\Shipment\Commands\ConsumeShipmentCommand;
use App\Services\Shipment\Commands\ImportShipmentsCommand;
use App\Services\Shipment\Concretes\Importer\ShipmentImporterServiceService;
use App\Services\Shipment\Concretes\Importer\ShipmentImportHistoryService;
use App\Services\Shipment\Concretes\ShipmentService;
use App\Services\Shipment\Contracts\Importer\ShipmentImporterServiceContract;
use App\Services\Shipment\Contracts\Importer\ShipmentImportHistoryRepositoryContract;
use App\Services\Shipment\Contracts\Importer\ShipmentImportHistoryServiceContract;
use App\Services\Shipment\Contracts\ShipmentRepositoryContract;
use App\Services\Shipment\Contracts\ShipmentServiceContract;
use App\Services\Shipment\Repositories\ShipmentEloquentRepository;
use App\Services\Shipment\Repositories\ShipmentImportHistoryEloquentRepository;
use Illuminate\Support\ServiceProvider;

class ShipmentServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(ShipmentRepositoryContract::class, ShipmentEloquentRepository::class);
        $this->app->bind(ShipmentServiceContract::class, ShipmentService::class);
    }
}
