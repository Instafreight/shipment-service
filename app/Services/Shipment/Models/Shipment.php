<?php

namespace App\Services\Shipment\Models;

use App\Services\Carrier\Models\Carrier;
use App\Services\Company\Models\Company;
use App\Services\Region\Models\City;
use App\Services\Route\Models\Route;
use App\Services\ShipmentImporter\Models\ShipmentImportHistory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Shipment extends Model
{
    use HasFactory;

    protected $fillable = ['distance', 'time', 'price', 'source_id', 'company_id', 'carrier_id', 'start_route_id', 'end_route_id'];

    public $timestamps = false;

    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    public function carrier(): BelongsTo
    {
        return $this->belongsTo(Carrier::class);
    }

    public function startRoute(): BelongsTo
    {
        return $this->belongsTo(Route::class, 'start_route_id', 'id');
    }

    public function endRoute(): BelongsTo
    {
        return $this->belongsTo(Route::class, 'end_route_id', 'id');
    }

    public function importHistories(): HasMany
    {
        return $this->hasMany(ShipmentImportHistory::class);
    }
}
