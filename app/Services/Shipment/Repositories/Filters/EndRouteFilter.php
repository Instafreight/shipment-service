<?php

namespace App\Services\Shipment\Repositories\Filters;

use App\Services\Base\Repository\Concretes\EloquentFilter;
use App\Services\Base\Repository\Contracts\CriteriaFilter;

class EndRouteFilter extends EloquentFilter implements CriteriaFilter
{
    protected function getRelation(): string
    {
        return 'endRoute';
    }
}
