<?php

namespace App\Services\Shipment\Repositories\Filters;

use App\Services\Base\Repository\Contracts\CriteriaFilter;

class StartRouteCountryFilter extends StartRouteFilter implements CriteriaFilter
{
    protected function getRelation(): string
    {
        return 'startRoute.city.country';
    }
}
