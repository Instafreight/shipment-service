<?php

namespace App\Services\Shipment\Repositories;

use App\Services\Base\Repository\Concretes\EloquentRepository;
use App\Services\Shipment\Contracts\ShipmentRepositoryContract;
use App\Services\Shipment\Models\Shipment;

class ShipmentEloquentRepository extends EloquentRepository implements ShipmentRepositoryContract
{
    public function __construct(Shipment $model)
    {
        parent::__construct($model);
    }

    public function findBySourceId(int $sourceId, array $columns = ['*'])
    {
        return $this->model->select($columns)->where('source_id', $sourceId)->first();
    }

    public function getRelations(): array
    {
        return ['company', 'carrier', 'startRoute.city.country', 'endRoute.city.country'];
    }
}
