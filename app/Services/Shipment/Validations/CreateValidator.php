<?php

namespace App\Services\Shipment\Validations;

use App\Services\Base\Validation\AbstractValidator;

class CreateValidator extends AbstractValidator
{
    public function rules(): array
    {
        return [
            "distance" => 'required|integer',
            "time" => 'required|integer',
            "price" => 'float',
            "company_id" => 'required|integer|exists:companies,id',
            "carrier_id" => 'required|integer|exists:carriers,id',
            "start_route_id" => 'required|integer|exists:routes,id',
            "end_route_id" => 'required|integer|exists:routes,id',
        ];
    }
}
