<?php

namespace App\Services\Shipment\Validations;

use App\Services\Base\Validation\AbstractValidator;

class UpdateValidator extends AbstractValidator
{
    public function rules(): array
    {
        return [
            "distance" => 'integer',
            "time" => 'integer',
            "price" => 'float',
            "company_id" => 'integer|exists:companies,id',
            "carrier_id" => 'integer|exists:carriers,id',
            "start_route_id" => 'integer|exists:routes,id',
            "end_route_id" => 'integer|exists:routes,id',
        ];
    }
}
