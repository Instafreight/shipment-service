<?php

namespace App\Services\Shipment\Concretes\Pricing;

use App\Services\Shipment\Contracts\PriceCalculationStrategyContract;

class PricingStrategyNormal extends PriceCalculationStrategyContract
{
    public function getUnitPrice(): float
    {
        return 0.25;
    }
}
