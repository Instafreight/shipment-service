<?php

namespace App\Services\Shipment\Concretes\Pricing;

use App\Services\Shipment\Contracts\PriceCalculationStrategyContract;

class PricingStrategyVeryLong extends PriceCalculationStrategyContract
{
    public function getUnitPrice(): float
    {
        return 0.15;
    }
}
