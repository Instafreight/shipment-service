<?php

namespace App\Services\Shipment\Concretes;

use App\Services\Base\Repository\Concretes\CriteriaCollection;
use App\Services\Base\Repository\Exceptions\EntityCreateException;
use App\Services\Base\Repository\Exceptions\EntityNotFoundException;
use App\Services\Base\Repository\Exceptions\EntityUpdateException;
use App\Services\Base\Repository\Traits\hasFilters;
use App\Services\Base\Validation\Exception\ValidationException;
use App\Services\Base\Validation\Traits\hasValidation;
use App\Services\Shipment\Concretes\Pricing\PricingStrategyLong;
use App\Services\Shipment\Concretes\Pricing\PricingStrategyNormal;
use App\Services\Shipment\Concretes\Pricing\PricingStrategyShort;
use App\Services\Shipment\Concretes\Pricing\PricingStrategyVeryLong;
use App\Services\Shipment\Contracts\ShipmentRepositoryContract;
use App\Services\Shipment\Contracts\ShipmentServiceContract;
use App\Services\Shipment\Exceptions\InvalidShipmentDistanceException;
use App\Services\Shipment\Repositories\Filters\CarrierFilter;
use App\Services\Shipment\Repositories\Filters\CompanyFilter;
use App\Services\Shipment\Repositories\Filters\EndRouteCityFilter;
use App\Services\Shipment\Repositories\Filters\EndRouteCountryFilter;
use App\Services\Shipment\Repositories\Filters\EndRouteFilter;
use App\Services\Shipment\Repositories\Filters\StartRouteCityFilter;
use App\Services\Shipment\Repositories\Filters\StartRouteCountryFilter;
use App\Services\Shipment\Repositories\Filters\StartRouteFilter;
use App\Services\Shipment\Validations\CreateValidator;
use App\Services\Shipment\Validations\UpdateValidator;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class ShipmentService implements ShipmentServiceContract
{
    use hasFilters;
    use hasValidation;

    private ShipmentRepositoryContract $repository;

    public function __construct(ShipmentRepositoryContract $shipmentRepository)
    {
        $this->repository = $shipmentRepository;
    }

    public function all()
    {
        return $this->repository->all();
    }

    public function allWithFilter(CriteriaCollection $criteria)
    {
        return $this->repository->filter($criteria)->all();
    }

    public function findById(int $id)
    {
        return $this->repository->findById($id);
    }

    public function findBySourceId(int $sourceId)
    {
        return $this->repository->findBySourceId($sourceId);
    }

    /**
     * @throws ValidationException
     * @throws EntityCreateException|InvalidShipmentDistanceException
     */
    public function create(array $data)
    {
//        $this->validate(CreateValidator::class, $data);
        $data['price'] = $this->calculateShipmentPrice($data['distance']);

        return $this->repository->create($data);
    }

    /**
     * @throws EntityNotFoundException
     * @throws ValidationException
     * @throws EntityUpdateException|InvalidShipmentDistanceException
     */
    public function update(int $id, array $data)
    {
//        $this->validate(UpdateValidator::class, $data);
        if(isset($data['distance'])) {
            $data['price'] = $this->calculateShipmentPrice($data['distance']);
        }

        return $this->repository->update($id, $data);
    }

    public function paginate($limit = 10, $offset = 1)
    {
        return $this->repository->paginate($limit, $offset);
    }

    public function paginateWithFilter($limit = 10, $offset = 1, array $filters = [])
    {
        $criteria = $this->extractFilters($filters);
        $this->repository->filter($criteria)->enableEagerLoadRelations();

        return $this->paginate($limit, $offset);
    }

    /**
     * @throws InvalidShipmentDistanceException
     */
    public function calculateShipmentPrice(int $distance): float
    {
        if ($distance < 0) {
            throw new InvalidShipmentDistanceException();
        } elseif ($distance <= 100) {
            $strategy = new PricingStrategyShort();
        } elseif ($distance <= 200) {
            $strategy = new PricingStrategyNormal();
        } elseif ($distance <= 300) {
            $strategy = new PricingStrategyLong();
        } else {
            $strategy = new PricingStrategyVeryLong();
        }

        return $strategy->calculate($distance);
    }

    public function getFilterMaps(): array
    {
        return [
            "company" => CompanyFilter::class,
            "carrier" => CarrierFilter::class,
            "start_route_city" => StartRouteCityFilter::class,
            "start_route_country" => StartRouteCountryFilter::class,
            "end_route_city" => EndRouteCityFilter::class,
            "end_route_country" => EndRouteCountryFilter::class,
            "start_route" => StartRouteFilter::class,
            "end_route" => EndRouteFilter::class,
        ];
    }
}
