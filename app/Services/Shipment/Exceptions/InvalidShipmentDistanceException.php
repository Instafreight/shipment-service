<?php

namespace App\Services\Shipment\Exceptions;

class InvalidShipmentDistanceException extends \App\Services\Base\Exception\AppException
{
    protected $message = "The specified distance is invalid";
}
