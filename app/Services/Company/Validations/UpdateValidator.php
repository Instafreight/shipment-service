<?php

namespace App\Services\Company\Validations;

use App\Services\Base\Validation\AbstractValidator;
use Illuminate\Validation\Rule;

class UpdateValidator extends AbstractValidator
{
    public function rules(): array
    {
        return [
            "name" => 'string',
            "email" => [
                'email',
                Rule::unique('companies')->ignore($this->entityId),
            ],
        ];
    }
}
