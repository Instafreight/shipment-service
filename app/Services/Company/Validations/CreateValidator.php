<?php

namespace App\Services\Company\Validations;

use App\Services\Base\Validation\AbstractValidator;

class CreateValidator extends AbstractValidator
{
    public function rules(): array
    {
        return [
            "name" => 'required|string',
            "email" => 'required|email|unique:companies,email',
        ];
    }
}
