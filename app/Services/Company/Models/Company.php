<?php

namespace App\Services\Company\Models;

use App\Services\Shipment\Models\Shipment;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Company extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'email'];

    public $timestamps = false;

    public function shipments(): HasMany
    {
        return $this->hasMany(Shipment::class);
    }
}
