<?php

namespace App\Services\Company;

use App\Services\Company\Concretes\CompanyService;
use App\Services\Company\Contracts\CompanyRepositoryContract;
use App\Services\Company\Contracts\CompanyServiceContract;
use App\Services\Company\Repositories\CompanyEloquentRepository;
use Illuminate\Support\ServiceProvider;

class CompanyServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(CompanyRepositoryContract::class, CompanyEloquentRepository::class);
        $this->app->bind(CompanyServiceContract::class, CompanyService::class);
    }
}
