<?php

namespace App\Services\Company\Contracts;

interface CompanyServiceContract
{
    public function all();

    public function findById(int $id);

    public function findByEmail(string $email);

    public function create(array $data);

    public function update(int $id, array $data);
}
