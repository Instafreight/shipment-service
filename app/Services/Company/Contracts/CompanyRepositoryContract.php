<?php

namespace App\Services\Company\Contracts;

use App\Services\Base\Repository\Contracts\AbstractRepositoryContract;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="Company",
 *     description="Company Repository",
 *     @OA\Xml(
 *         name="Company"
 *     ),
 *     @OA\Property(
 *          property="id",
 *          title="ID",
 *          description="ID of the Company",
 *          format="integer"
 *     ),
 *     @OA\Property(
 *          property="name",
 *          title="Name",
 *          description="name of the company",
 *          format="string"
 *     ),
 *     @OA\Property(
 *          property="email",
 *          title="Email",
 *          description="email of the company",
 *          format="string"
 *     )
 * )
 */
interface CompanyRepositoryContract extends AbstractRepositoryContract
{
    /**
     * Find company entity by email.
     *
     * @param string $email
     * @param array $columns
     */
    public function findByEmail(string $email, array $columns = ['*']);
}
