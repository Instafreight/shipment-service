<?php

namespace App\Services\Company\Repositories;

use App\Services\Base\Repository\Concretes\EloquentRepository;
use App\Services\Company\Contracts\CompanyRepositoryContract;
use App\Services\Company\Models\Company;

class CompanyEloquentRepository extends EloquentRepository implements CompanyRepositoryContract
{
    public function __construct(Company $model)
    {
        parent::__construct($model);
    }

    public function findByEmail(string $email, array $columns = ['*'])
    {
        return $this->model->select($columns)->where('email', $email)->first();
    }
}
