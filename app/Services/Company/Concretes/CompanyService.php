<?php

namespace App\Services\Company\Concretes;

use App\Services\Base\Repository\Exceptions\EntityCreateException;
use App\Services\Base\Repository\Exceptions\EntityNotFoundException;
use App\Services\Base\Repository\Exceptions\EntityUpdateException;
use App\Services\Base\Validation\Exception\ValidationException;
use App\Services\Base\Validation\Traits\hasValidation;
use App\Services\Company\Contracts\CompanyRepositoryContract;
use App\Services\Company\Contracts\CompanyServiceContract;
use App\Services\Company\Validations\CreateValidator;
use App\Services\Company\Validations\UpdateValidator;
use Illuminate\Database\Eloquent\Collection;

class CompanyService implements CompanyServiceContract
{
    use hasValidation;

    private CompanyRepositoryContract $repository;

    public function __construct(CompanyRepositoryContract $companyRepository)
    {
        $this->repository = $companyRepository;
    }

    public function all(): Collection
    {
        return $this->repository->all();
    }

    public function findById(int $id)
    {
        return $this->repository->findById($id);
    }

    public function findByEmail(string $email)
    {
        return $this->repository->findByEmail($email);
    }

    /**
     * @throws ValidationException
     * @throws EntityCreateException
     */
    public function create(array $data)
    {
        $this->validate(CreateValidator::class, $data);

        return $this->repository->create($data);
    }

    /**
     * @throws ValidationException
     * @throws EntityNotFoundException
     * @throws EntityUpdateException
     */
    public function update(int $id, array $data)
    {
        $this->validate(UpdateValidator::class, $data);

        return $this->repository->update($id, $data);
    }
}
