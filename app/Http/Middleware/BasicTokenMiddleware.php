<?php

namespace App\Http\Middleware;

use Closure;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @OA\SecurityScheme(
 *     securityScheme="BasicTokenAuth",
 *     type="http",
 *     description="Login with basic token",
 *     name="X-INSTAFREIGHT-KEY",
 *     type="apiKey",
 *     in="header"
 * ),
 * @OA\Response(
 *     response="Unauthorized",
 *     description="Authorization failure message",
 *     @OA\JsonContent(
 *          @OA\Property(
 *             description="Error Code",
 *             property="code",
 *             default="auth-0043",
 *             type="string"
 *          ),
 *          @OA\Property(
 *             description="Error Message",
 *             property="message",
 *             default="This action is unauthorized.",
 *             type="string"
 *          ),
 *          @OA\Property(
 *             description="Error Type",
 *             property="type",
 *             default="AccessDeniedException",
 *             type="string"
 *          ),
 *          @OA\Property(
 *             description="Error detail",
 *             property="detail",
 *             default="Ensure you have proper access or contact your admin to get access.",
 *             type="string"
 *          )
 *     )
 * )
 */
class BasicTokenMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $apiKey = $request->header('X-INSTAFREIGHT-KEY',null);
        if($apiKey === null){
            throw new AccessDeniedHttpException();
        }

        if($apiKey !== config('auth.basic.token')){
            throw new AccessDeniedHttpException();
        }
        return $next($request);
    }
}
