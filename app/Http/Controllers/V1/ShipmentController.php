<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Shipment\ImportRequest;
use App\Http\Requests\Shipment\ListRequest;
use App\Services\Base\Validation\Exception\ValidationException;
use App\Services\Base\Validation\JsonSchemaValidator;
use App\Services\ShipmentImporter\Contracts\ShipmentImporterServiceContract;
use App\Services\Shipment\Contracts\ShipmentServiceContract;
use App\Services\ShipmentImporter\Contracts\ShipmentProducerContract;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Str;
use JsonMachine\Items;
use OpenApi\Annotations as OA;

/**
 * @OA\Info(
 *     version="1.0",
 *     title="Shipment Service",
 *      @OA\Contact(
 *          name="Mohsen Nazari",
 *          email="mohsennazari90@gmail.com",
 *          url="http://mohsen.codes"
 *      )
 * )
 */
class ShipmentController extends Controller
{
    private ShipmentServiceContract $shipmentService;
    public function __construct(ShipmentServiceContract $shipmentService)
    {
        $this->shipmentService = $shipmentService;
    }

    /**
     * @OA\Get(
     *     path="/api/v1/shipments",
     *     description="Filter the paginated list of shipments",
     *     @OA\Parameter(ref="#/components/parameters/carrier_email"),
     *     @OA\Parameter(ref="#/components/parameters/carrier_name"),
     *     @OA\Parameter(ref="#/components/parameters/company_email"),
     *     @OA\Parameter(ref="#/components/parameters/company_name"),
     *     @OA\Parameter(ref="#/components/parameters/end_route_city_name"),
     *     @OA\Parameter(ref="#/components/parameters/end_route_country_name"),
     *     @OA\Parameter(ref="#/components/parameters/end_route_country_alpha2"),
     *     @OA\Parameter(ref="#/components/parameters/end_route_post_code"),
     *     @OA\Parameter(ref="#/components/parameters/start_route_city_name"),
     *     @OA\Parameter(ref="#/components/parameters/start_route_country_name"),
     *     @OA\Parameter(ref="#/components/parameters/start_route_country_alpha2"),
     *     @OA\Parameter(ref="#/components/parameters/start_route_post_code"),
     *     @OA\Response(response=200, description="test", ref = "#/components/responses/ShipmentPaginator"),
     *     @OA\Response(response=422, ref="#/components/responses/Unprocessable")
     * )
     */
    public function index(ListRequest $request)
    {
        $pagination = [
            "page" => $request->query('page', 1),
            "per_page" => $request->query('per_page', config('shipment.pagination.per_page')),
        ];

        $filters = [];
        foreach($request->all() as $name => $val) {
            if(Str::startsWith($name, array_keys($this->shipmentService->getFilterMaps()))) {
                $filters [$name] = $val;
            }
        }

        return $this->shipmentService->paginateWithFilter($pagination['per_page'], $pagination['page'], $filters);
    }

    /**
     * @OA\Post(
     *     path="/api/v1/shipments/import",
     *     description="Import list of shipments from file upload (Async)",
     *     security={{"BasicTokenAuth":{}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 allOf={
     *                     @OA\Schema(
     *                         @OA\Property(
     *                             description="Shipments file",
     *                             property="import_file",
     *                             type="string", format="binary"
     *                         )
     *                     )
     *                 }
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="Return number of queued elements extracted from file",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  description="Number of queued elements",
     *                  property="queuedCount",
     *                  type="integer"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=403, ref="#/components/responses/Unauthorized"),
     *     @OA\Response(response=422, ref="#/components/responses/Unprocessable")
     *  )
     */
    public function import(ImportRequest $request): JsonResponse
    {
        $file = $request->file('import_file');
        $shipments = Items::fromFile($file->getRealPath());

        $counter = 0;
        foreach ($shipments as $shipment) {
            $counter++;
            (app()->make(ShipmentProducerContract::class))->dispatch($shipment);
        }

        return response()->json([
            "queued_count" => $counter
        ]);
    }

    /**
     * @OA\Post(
     *     path="/api/v1/shipments/import-sync",
     *     description="Import list of shipments from file upload (Sync)",
     *     security={{"BasicTokenAuth":{}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 allOf={
     *                     @OA\Schema(
     *                         @OA\Property(
     *                             description="Shipments file",
     *                             property="import_file",
     *                             type="string", format="binary"
     *                         )
     *                     )
     *                 }
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="Return number of imported elements extracted from file",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  description="Number of imported elements",
     *                  property="importedCount",
     *                  type="integer"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=403, ref="#/components/responses/Unauthorized"),
     *     @OA\Response(response=422, ref="#/components/responses/Unprocessable")
     *  )
     */
    public function importSync(ImportRequest $request): JsonResponse
    {
        $file = $request->file('import_file');
        $shipments = Items::fromFile($file->getRealPath());
        $shipmentImporter = app()->make(ShipmentImporterServiceContract::class);

        $jsonValidator = app()->make(JsonSchemaValidator::class);
        $schemaPath = config('shipment.importer.validation.json_schema_path');

        $counter = 0;
        foreach ($shipments as $shipment) {
            try {
                if(!$jsonValidator->validate($schemaPath, $shipment)) {
                    continue;
                }
                $shipmentImporter->import($shipment);
                $counter++;
            } catch (\Exception $e) {
                continue;
            }
        }

        return response()->json([
            "importedCount" => $counter
        ]);
    }
}
