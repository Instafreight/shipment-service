<?php

namespace App\Http\Requests\Shipment;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;

/**
 * @OA\Response(
 *     response="Unprocessable",
 *     description="Validation failure",
 *     @OA\JsonContent(
 *          @OA\Property(
 *             description="Error Code",
 *             property="code",
 *             default="validation-0001",
 *             type="string"
 *          ),
 *          @OA\Property(
 *             description="Error Message",
 *             property="message",
 *             type="string"
 *          ),
 *          @OA\Property(
 *             description="Error Type",
 *             property="type",
 *             default="ValidationException",
 *             type="string"
 *          ),
 *          @OA\Property(
 *             description="Error detail",
 *             property="detail",
 *             type="object",
 *             @OA\Property(
 *                  description="Input Name",
 *                  property="input_name",
 *                  type="array",
 *                  @OA\Items(
 *                      description="Input Name",
 *                      type="string",
 *                      example="error string"
 *                  )
 *             ),
 *          )
 *      )
 * )
 */
class ImportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            "import_file" => "required|file|mimes:json"
        ];
    }
}
