<?php

namespace App\Http\Requests\Shipment;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;

/**
 *     @OA\Parameter(
 *         description="Email of the Carrier",
 *         in="query",
 *         name="carrier_email",
 *         required=false,
 *         @OA\Schema(type="string"),
 *     ),
 *     @OA\Parameter(
 *         description="Name of the Carrier",
 *         in="query",
 *         name="carrier_name",
 *         required=false,
 *         @OA\Schema(type="string"),
 *     ),
 *     @OA\Parameter(
 *         description="Email of the Company",
 *         in="query",
 *         name="company_email",
 *         required=false,
 *         @OA\Schema(type="string"),
 *     ),
 *     @OA\Parameter(
 *         description="Name of the Company",
 *         in="query",
 *         name="company_name",
 *         required=false,
 *         @OA\Schema(type="string"),
 *     ),
 *     @OA\Parameter(
 *         description="Name of the ending route city",
 *         in="query",
 *         name="end_route_city_name",
 *         required=false,
 *         @OA\Schema(type="string"),
 *     ),
 *     @OA\Parameter(
 *         description="Name of the ending route country",
 *         in="query",
 *         name="end_route_country_name",
 *         required=false,
 *         @OA\Schema(type="string"),
 *     ),
 *     @OA\Parameter(
 *         description="Alpha2 code of the ending route country",
 *         in="query",
 *         name="end_route_country_alpha2",
 *         required=false,
 *         @OA\Schema(type="string"),
 *     ),
 *     @OA\Parameter(
 *         description="Postal code of the ending route",
 *         in="query",
 *         name="end_route_post_code",
 *         required=false,
 *         @OA\Schema(type="string"),
 *     ),
 *     @OA\Parameter(
 *         description="Name of the starting route city",
 *         in="query",
 *         name="start_route_city_name",
 *         required=false,
 *         @OA\Schema(type="string"),
 *     ),
 *     @OA\Parameter(
 *         description="Name of the starting route country",
 *         in="query",
 *         name="start_route_country_name",
 *         required=false,
 *         @OA\Schema(type="string"),
 *     ),
 *     @OA\Parameter(
 *         description="Alpha2 code of the starting route country",
 *         in="query",
 *         name="start_route_country_alpha2",
 *         required=false,
 *         @OA\Schema(type="string"),
 *     ),
 *     @OA\Parameter(
 *         description="Postal code of the starting route",
 *         in="query",
 *         name="start_route_post_code",
 *         required=false,
 *         @OA\Schema(type="string"),
 *     )
 */
class ListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            "carrier_email" => "email",
            "carrier_name" => "string",
            "company_email" => "email",
            "company_name" => "string",
            "end_route_city_name" => "string",
            "end_route_country_name" => "string",
            "end_route_country_alpha2" => "string|size:2",
            "end_route_post_code" => "string",
            "start_route_city_name" => "string",
            "start_route_country_name" => "string",
            "start_route_country_alpha2" => "string|size:2",
            "start_route_post_code" => "string",
        ];
    }
}
